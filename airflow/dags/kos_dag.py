from __future__ import annotations

from datetime import datetime, timedelta

import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.dummy_operator import DummyOperator


import pandas as pd

import sys
import os

from custom_operators.load_to_stage_operator import LoadToStageOperator
from custom_operators.make_increment_operator import MakeIncrementOperator
from custom_operators.load_to_target_operator import LoadToTargetOperator

from custom_operators.helpers.dwh_email import *

from kos.kos_config import *

import logging

#===============================================================================
def check_trigger(**kwargs):
    dag_trigger_name = kwargs['dag_run'].conf.get('dag_trigger_name', False)
    if dag_trigger_name == 'ezop_dag':
        return 'task_ezop_triggered'
    elif dag_trigger_name == 'usermap_dag':
        return 'task_usermap_triggered'
    else: return 'task_auto_triggered'

#===============================================================================

DAG_NAME = "kos_dag"
default_args = {
    "owner": "zolockri",
    "depends_on_past": False,
    "start_date": pendulum.datetime(2021, 1, 1, tz="UTC"),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    "execution_timeout": timedelta(minutes=90)
}
with DAG(
    dag_id=DAG_NAME,
    default_args=default_args,
    schedule="0 0 * * *",
    catchup=False,
    # dagrun_timeout=datetime.timedelta(minutes=60)
) as dag:

    task_start_kos = PythonOperator(
        task_id="start_kos",
        python_callable=send_email,
        op_kwargs={'subject': 'Airflow: KOS loading started',
                   'message': f"KOS loading started at {datetime.now()}"}
    )
# #===============================================================================
# #=================================BRANCHING=====================================
# #===============================================================================

    task_trigger_check = BranchPythonOperator(
        task_id="task_trigger_check",
        python_callable=check_trigger,
        provide_context=True,
        dag=dag
    )

# #===============================================================================
    task_ezop_triggered = DummyOperator(
        task_id="task_ezop_triggered",
        dag=dag
    )

    task_usermap_triggered = DummyOperator(
        task_id="task_usermap_triggered",
        dag=dag
    )

    task_auto_triggered = DummyOperator(
        task_id="task_auto_triggered",
        dag=dag
    )


#===============================================================================
#================================TO STAGE=======================================
#===============================================================================

#==============================TASK LOAD tusers===============================

    task_load_tusers = LoadToStageOperator(
                task_id="task_load_tusers",
                src_conn_id=KOS_SRC_CONN_ID,
                src_db=KOS_SRC_DB,
                src_schema=KOS_SRC_SCHEMA,
                src_table=KOS_SRC_TABLE_TUSERS,
                src_columns=KOS_SRC_COLUMNS_TUSERS,
                src_array_size=5*10**5,
                stg_conn_id=KOS_STG_CONN_ID,
                stg_db=KOS_STG_DB,
                stg_schema=KOS_STG_SCHEMA,
                stg_table=KOS_STG_TABLE_TUSERS,
                stg_columns=KOS_STG_COLUMNS_TUSERS,
                stg_columns_dtype=KOS_STG_COLUMNS_DTYPES_TUSERS,
                trigger_rule='none_failed_or_skipped'
    )

#==========================TASK LOAD TKONTAKT===================================

    task_load_tkontakt = LoadToStageOperator(
                task_id="task_load_tkontakt",
                src_conn_id=KOS_SRC_CONN_ID,
                src_db=KOS_SRC_DB,
                src_schema=KOS_SRC_SCHEMA,
                src_table=KOS_SRC_TABLE_TKONTAKT,
                src_columns=KOS_SRC_COLUMNS_TKONTAKT,
                src_array_size=5*10**5,
                stg_conn_id=KOS_STG_CONN_ID,
                stg_db=KOS_STG_DB,
                stg_schema=KOS_STG_SCHEMA,
                stg_table=KOS_STG_TABLE_TKONTAKT,
                stg_columns=KOS_STG_COLUMNS_TKONTAKT,
                stg_columns_dtype=KOS_STG_COLUMNS_DTYPES_TKONTAKT
    )
#===========================TASK LOAD TEKNS=====================================

    task_load_tekns = LoadToStageOperator(
                task_id="task_load_tekns",
                src_conn_id=KOS_SRC_CONN_ID,
                src_db=KOS_SRC_DB,
                src_schema=KOS_SRC_SCHEMA,
                src_table=KOS_SRC_TABLE_TEKNS,
                src_columns=KOS_SRC_COLUMNS_TEKNS,
                src_array_size=5*10**5,
                stg_conn_id=KOS_STG_CONN_ID,
                stg_db=KOS_STG_DB,
                stg_schema=KOS_STG_SCHEMA,
                stg_table=KOS_STG_TABLE_TEKNS,
                stg_columns=KOS_STG_COLUMNS_TEKNS,
                stg_columns_dtype=KOS_STG_COLUMNS_DTYPES_TEKNS,
                trigger_rule='none_failed_or_skipped'
    )


# ===============================================================================
# ================================INCREMENT======================================
# ===============================================================================

# =====================TASK MAKE INCREMENT TKONTAKT=============================

    task_make_increment_tkontakt = MakeIncrementOperator(
        task_id="task_make_increment_tkontakt",
        source_name=KOS_SRC_NAME,
        table_name=KOS_STG_TABLE_TKONTAKT,
        conn_id=KOS_STG_CONN_ID,
        db=KOS_STG_DB,
        is_psc=True
    )

# =======================TASK MAKE INCREMENT TEKNS==============================

    task_make_increment_tekns = MakeIncrementOperator(
        task_id="task_make_increment_tekns",
        source_name=KOS_SRC_NAME,
        table_name=KOS_STG_TABLE_TEKNS,
        conn_id=KOS_STG_CONN_ID,
        db=KOS_STG_DB,
        is_psc=True
    )

# ===============================================================================
# =================================TARGET========================================
# ===============================================================================

# ========================TASK LOAD T_KOUD_ADRESA===============================

    task_load_t_koud_adresa = LoadToTargetOperator(
        task_id="task_load_t_koud_adresa",
        #===conections===
        stg_conn_id=KOS_STG_CONN_ID,
        stg_db=KOS_STG_DB,
        tg_conn_id=KOS_TG_CONN_ID,
        tg_db=KOS_TG_DB,
        #===table info===
        si_schema=KOS_SI_SCHEMA,
        si_table=KOS_SI_TABLE_TKONTAKT_ADRESA,
        si_columns=KOS_SI_COLUMNS_TKONTAKT_ADRESA,
        tg_schema=KOS_TG_SCHEMA,
        tg_table=KOS_TG_TABLE_KOUD_ADRESA,
        tg_columns=KOS_TG_COLUMNS_KOUD_ADRESA,
        #===info for transforamtions===
        columns_to_concat=KOS_TG_COLUMNS_TO_CONCAT_KOUD_ADRESA,
        tg_dtypes=KOS_TG_COLUMNS_DTYPES_KOUD_ADRESA,
        str_columns=KOS_TG_COLUMNS_STRING_KOUD_ADRESA,
        timestamp_columns=KOS_TG_COLUMNS_TIMESTAMP_KOUD_ADRESA,
        init_load=False,
        bk_en_cz=KOS_TG_BK_EN_CZ_KOUD_ADRESA,
        process_data=None
    )

#=================TASK LOAD T_KOUD_TELEFONNI_CISLO==========================

    task_load_t_koud_telefonni_cislo = LoadToTargetOperator(
        task_id="task_load_t_koud_telefonni_cislo",
        #===conections===
        stg_conn_id=KOS_STG_CONN_ID,
        stg_db=KOS_STG_DB,
        tg_conn_id=KOS_TG_CONN_ID,
        tg_db=KOS_TG_DB,
        #===table info===
        si_schema=KOS_SI_SCHEMA,
        si_table=KOS_SI_TABLE_TKONTAKT_TELEFONNI_CISLO,
        si_columns=KOS_SI_COLUMNS_TKONTAKT_TELEFONNI_CISLO,
        tg_schema=KOS_TG_SCHEMA,
        tg_table=KOS_TG_TABLE_KOUD_TELEFONNI_CISLO,
        tg_columns=KOS_TG_COLUMNS_KOUD_TELEFONNI_CISLO,
        #===info for transforamtions===
        columns_to_concat=KOS_TG_COLUMNS_TO_CONCAT_KOUD_TELEFONNI_CISLO,
        tg_dtypes=KOS_TG_COLUMNS_DTYPES_KOUD_TELEFONNI_CISLO,
        str_columns=KOS_TG_COLUMNS_STRING_KOUD_TELEFONNI_CISLO,
        timestamp_columns=KOS_TG_COLUMNS_TIMESTAMP_KOUD_TELEFONNI_CISLO,
        init_load=False,
        bk_en_cz=KOS_TG_BK_EN_CZ_KOUD_TELEFONNI_CISLO,
        process_data=None
    )

#=================TASK LOAD T_ORG_ORGANIZACNI_JEDNOTKA==========================

    task_load_t_org_organizacni_jednotka = LoadToTargetOperator(
        task_id="task_load_t_org_organizacni_jednotka",
        #===conections===
        stg_conn_id=KOS_STG_CONN_ID,
        stg_db=KOS_STG_DB,
        tg_conn_id=KOS_TG_CONN_ID,
        tg_db=KOS_TG_DB,
        #===table info===
        si_schema=KOS_SI_SCHEMA,
        si_table=KOS_SI_TABLE_TEKNS,
        si_columns=KOS_SI_COLUMNS_TEKNS,
        tg_schema=KOS_TG_SCHEMA,
        tg_table=KOS_TG_TABLE_ORGANIZACNI_JEDNOTKA,
        tg_columns=KOS_TG_COLUMNS_ORGANIZACNI_JEDNOTKA,
        #===info for transforamtions===
        columns_to_concat=KOS_TG_COLUMNS_TO_CONCAT_ORGANIZACNI_JEDNOTKA,
        tg_dtypes=KOS_TG_COLUMNS_DTYPES_ORGANIZACNI_JEDNOTKA,
        str_columns=KOS_TG_COLUMNS_STRING_ORGANIZACNI_JEDNOTKA,
        timestamp_columns=KOS_TG_COLUMNS_TIMESTAMP_ORGANIZACNI_JEDNOTKA,
        init_load=False,
        bk_en_cz=KOS_TG_BK_EN_CZ_ORGANIZACNI_JEDNOTKA,
        process_data=None
    )


#==========================TASK LOAD T_KOUD_EMAIL===============================

    task_load_t_koud_email = LoadToTargetOperator(
        task_id="task_load_t_koud_email",
        #===conections===
        stg_conn_id=KOS_STG_CONN_ID,
        stg_db=KOS_STG_DB,
        tg_conn_id=KOS_TG_CONN_ID,
        tg_db=KOS_TG_DB,
        #===table info===
        si_schema=KOS_SI_SCHEMA,
        si_table=KOS_SI_TABLE_TKONTAKT_EMAIL,
        si_columns=KOS_SI_COLUMNS_TKONTAKT_EMAIL,
        tg_schema=KOS_TG_SCHEMA,
        tg_table=KOS_TG_TABLE_KOUD_EMAIL,
        tg_columns=KOS_TG_COLUMNS_KOUD_EMAIL,
        #===info for transforamtions===
        columns_to_concat=KOS_TG_COLUMNS_TO_CONCAT_KOUD_EMAIL,
        tg_dtypes=KOS_TG_COLUMNS_DTYPES_KOUD_EMAIL,
        str_columns=KOS_TG_COLUMNS_STRING_KOUD_EMAIL,
        timestamp_columns=KOS_TG_COLUMNS_TIMESTAMP_KOUD_EMAIL,
        init_load=False,
        bk_en_cz=KOS_TG_BK_EN_CZ_KOUD_EMAIL,
        process_data=None
    )


#===============================================================================
#===============================================================================
    #
    task_start_kos >> task_trigger_check
    task_trigger_check >> [task_usermap_triggered,
                           task_ezop_triggered,
                           task_auto_triggered]

    task_usermap_triggered >> task_load_tusers

    task_ezop_triggered >> task_load_tekns

    task_auto_triggered >> [task_load_tusers,
                          task_load_tkontakt,
                          task_load_tekns]
    task_load_tkontakt >> task_make_increment_tkontakt
    task_load_tekns >> task_make_increment_tekns
    task_make_increment_tkontakt >> [task_load_t_koud_adresa,
                                     task_load_t_koud_email,
                                     task_load_t_koud_telefonni_cislo]
    task_make_increment_tekns >> task_load_t_org_organizacni_jednotka

    # task_load_t_koud_adresa
    # task_load_t_koud_email
    # task_load_t_koud_telefonni_cislo
    # task_load_t_org_organizacni_jednotka

    # task_start_grades >> [task_load_student_classification, task_load_t_klas_klasifikace]
    # task_load_student_classification >> task_make_increment_student_classification >> task_load_t_klas_klasifikace_student
# if __name__ == "__main__":
#     dag.test()
