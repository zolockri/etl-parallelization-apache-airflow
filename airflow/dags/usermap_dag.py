from __future__ import annotations

from datetime import datetime, timedelta
import time

import pendulum

from airflow import DAG, settings
from airflow.models import DagRun
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.subdag_operator import SubDagOperator
from airflow.models.taskinstance import TaskInstance
from airflow.utils.state import State
from sqlalchemy import func

import pandas as pd

import sys
import os

from custom_operators.load_to_stage_operator import LoadToStageOperator
from custom_operators.load_to_stage_operator import LoadToStageOperator
from custom_operators.make_increment_operator import MakeIncrementOperator
from custom_operators.load_to_target_operator import LoadToTargetOperator

from custom_operators.helpers.dwh_email import *

from usermap.usermap_config import *
from target_transformations import process_data_osoba
import logging

def get_most_recent_dag_run(dag_id):
    dag_runs = DagRun.find(dag_id=dag_id)
    dag_runs.sort(key=lambda x: x.execution_date, reverse=True)
    return dag_runs[0] if dag_runs else None

def print_date():
    dag_run = get_most_recent_dag_run('kos_dag')
    if dag_run:
        print(f'The most recent DagRun was executed at: {dag_run.execution_date}')

def get_last_success_date(dag_id, task_id, **kwargs):
    # ti = kwargs['ti']
    # current_datetime = ti.xcom_pull(key='custom_current_datetime', task_ids=task_id)
    # print("Current datetime from XCom:", current_datetime)
    # return current_datetime

    session = settings.Session()

    # Get the latest successful TaskInstance
    task_instance = (
        session.query(TaskInstance)
        .filter(TaskInstance.dag_id == dag_id, TaskInstance.task_id == task_id, TaskInstance.state == State.SUCCESS)
        .join(TaskInstance.dag_run)
        .order_by(TaskInstance.execution_date.desc())
        .first()
    )

    session.close()
    if task_instance:
        now = datetime.now(timezone.utc)
        last_successful_execution_date = task_instance.execution_date.replace(tzinfo=timezone.utc)
        return last_successful_execution_date.date()

def wait_tekusers_done(**kwargs):
    while get_last_success_date("kos_dag", "task_load_tusers") != now.date():
        time.sleep(5)
    return

def task_is_tekusers_done(**kwargs):
    if get_last_success_date("kos_dag", "task_load_tusers") != now.date():
        return 'task_trigger_tusers'
    else: return 'task_load_t_osob_osoba'


DAG_NAME = "usermap_dag"
default_args = {
    "owner": "zolockri",
    "depends_on_past": False,
    "start_date": pendulum.datetime(2021, 1, 1, tz="UTC"),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    "execution_timeout": timedelta(minutes=90)
}
with DAG(
    dag_id=DAG_NAME,
    default_args=default_args,
    schedule="0 0 * * *",
    catchup=False,
    # dagrun_timeout=datetime.timedelta(minutes=60)
) as dag:
    # 1. SUBDAG: Extract data to stage

    # task_start_grades = EmptyOperator(
    #     task_id="start_grades"
    # )

    task_start_usermap = PythonOperator(
        task_id="task_start_usermap",
        python_callable=send_email,
        op_kwargs={'subject': 'Airflow: USERMAP loading started',
                   'message': f"USERMAP loading started at {datetime.now()}"}
    )

#===============================================================================
#================================TO STAGE=======================================
#===============================================================================

#================================TASK LOAD OSOBY================================

    task_load_osoby = LoadToStageOperator(
                task_id="task_load_osoby",
                src_conn_id=USERMAP_SRC_CONN_ID,
                src_db=USERMAP_SRC_DB,
                src_schema=USERMAP_SRC_SCHEMA,
                src_table=USERMAP_SRC_TABLE_OSOBY,
                src_columns=USERMAP_SRC_COLUMNS_OSOBY,
                src_array_size=10**5,
                stg_conn_id=USERMAP_STG_CONN_ID,
                stg_db=USERMAP_STG_DB,
                stg_schema=USERMAP_STG_SCHEMA,
                stg_table=USERMAP_STG_TABLE_OSOBY,
                stg_columns=USERMAP_STG_COLUMNS_OSOBY,
                stg_columns_dtype=USERMAP_STG_COLUMNS_DTYPES_OSOBA
    )

# ==============================================================================
# ================================INCREMENT=====================================
# ==============================================================================

# ========================TASK MAKE INCREMENT OSOBY=============================

    task_make_increment_osoby = MakeIncrementOperator(
        task_id="task_make_increment_osoby",
        source_name=USERMAP_SRC_NAME,
        table_name=USERMAP_STG_TABLE_OSOBY,
        conn_id=USERMAP_STG_CONN_ID,
        db=USERMAP_STG_DB,
        is_psc=True
    )

# ==============================================================================
# ================================DEPENDENCIES==================================
# ==============================================================================

    # task_is_tekusers_done = BranchPythonOperator(
    #     task_id='task_is_tekusers_done',
    #     python_callable=task_is_tekusers_done,
    #     provide_context=True,
    #     dag=dag
    # )
    #
    # task_get_tekusers_done = PythonOperator(
    #     task_id='task_get_tekusers_done',
    #     python_callable=wait_tekusers_done,
    #     provide_context=True,
    #     dag=dag
    # )
    #
    # task_trigger_kos = TriggerDagRunOperator(
    #     task_id='task_trigger_kos',
    #     trigger_dag_id='kos_dag',
    #     conf={'dag_trigger_name': 'usermap_dag'},
    #     dag=dag
    # )
    # task_print = PythonOperator(
    #     task_id='task_print',
    #     python_callable=print_date,
    #     provide_context=True,
    #     dag=dag
    # )


# ===============================================================================
# =================================TARGET========================================
# ===============================================================================

# ========================TASK LOAD T_OSOB_OSOBA================================

    task_load_t_osob_osoba = LoadToTargetOperator(
        task_id="task_load_t_osob_osoba",
        #===conections===
        stg_conn_id=USERMAP_STG_CONN_ID,
        stg_db=USERMAP_STG_DB,
        tg_conn_id=USERMAP_TG_CONN_ID,
        tg_db=USERMAP_TG_DB,
        #===table info===
        si_schema=USERMAP_SI_SCHEMA,
        si_table=USERMAP_SI_TABLE_OSOBA,
        si_columns=USERMAP_SI_COLUMNS_OSOBA,
        tg_schema=USERMAP_TG_SCHEMA,
        tg_table=USERMAP_TG_TABLE_OSOBA,
        tg_columns=USERMAP_TG_COLUMNS_OSOBA,
        #===info for transforamtions===
        columns_to_concat=USERMAP_TG_COLUMNS_TO_CONCAT_OSOBA,
        tg_dtypes=USERMAP_TG_COLUMNS_DTYPES_OSOBA,
        str_columns=USERMAP_TG_COLUMNS_STRING_OSOBA,
        timestamp_columns=USERMAP_TG_COLUMNS_TIMESTAMP_OSOBA,
        init_load=True,
        bk_en_cz=USERMAP_TG_BK_EN_CZ_OSOBA,
        process_data=process_data_osoba.process_data
    )

# ===============================================================================

    # task_start_usermap >> task_load_osoby >> task_make_increment_osoby >> task_is_tekusers_done >> [task_trigger_kos, task_load_t_osob_osoba]
    # task_start_usermap >> task_is_tekusers_done >> [task_trigger_kos, task_load_t_osob_osoba]
    # task_trigger_kos >> task_get_tekusers_done >> task_load_t_osob_osoba



    task_start_usermap >> task_load_osoby >> task_make_increment_osoby >> task_load_t_osob_osoba

    # task_print >> task_load_t_osob_osoba
