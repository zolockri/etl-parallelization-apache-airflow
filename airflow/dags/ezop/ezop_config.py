from sqlalchemy.sql.sqltypes import *

#============================CONNECTIONS INFO===================================
EZOP_SRC_CONN_ID = EZOP_SRC_NAME = 'ezop'
EZOP_SRC_DB = None
EZOP_SRC_SCHEMA = 'VVVS_REST'

EZOP_STG_CONN_ID = EZOP_TG_CONN_ID = 'dv5'
EZOP_STG_DB = 'dwh3_stage_kika'
EZOP_STG_SCHEMA = 'ps_ezop'

EZOP_TG_DB = 'dwh3_target_kika'
EZOP_TG_SCHEMA = 'dwh'

#===============================================================================
#===============================STAGE CONFIGURATIONS============================
#===============================================================================

#========================TCITATION_AFFILIATIONS=================================

EZOP_SRC_TABLE_TCITATION_AFFILIATIONS = 'TCITATION_AFFILIATIONS'
EZOP_SRC_COLUMNS_TCITATION_AFFILIATIONS = [ 'CITATION_AFFILIATION',
                                            'CITATION_AUTHOR',
                                            'AFFILIATION',
                                            'RANK',
                                            'WHO_CHANGED',
                                            'WHEN_CHANGED',
                                            'VALID']

EZOP_STG_TABLE_TCITATION_AFFILIATIONS = 'tcitation_affiliations'
EZOP_STG_COLUMNS_TCITATION_AFFILIATIONS = ['citation_affiliation_bk',
                                           'citation_author',
                                           'affiliation',
                                           'rank',
                                           'who_changed',
                                           'when_changed',
                                           'valid',
                                           'md5']
EZOP_STG_COLUMNS_DTYPES_TCITATION_AFFILIATIONS = {'citation_affiliation_bk': 'int64',
                                                   'citation_author': 'int64',
                                                   'affiliation': 'int64',
                                                   'rank': 'int32',
                                                   'who_changed': 'int32',
                                                   'when_changed': 'datetime64[ns]',
                                                   'valid': 'object'}
#============================TCITATION_AUTHORS==================================

EZOP_SRC_TABLE_TCITATION_AUTHORS = 'TCITATION_AUTHORS'
EZOP_SRC_COLUMNS_TCITATION_AUTHORS = ['CITATION_AUTHOR',
                                      'CITATION',
                                      'RANK',
                                      'SURNAME',
                                      'INITIALS',
                                      'NAME',
                                      'WHO_CHANGED',
                                      'WHEN_CHANGED',
                                      'VALID',
                                      'ORCID',
                                      'SCOPUS_AUTHOR_ID',
                                      'RESEARCHERID']

EZOP_STG_TABLE_TCITATION_AUTHORS = 'tcitation_authors'
EZOP_STG_COLUMNS_TCITATION_AUTHORS = ['citation_author_bk',
                                      'citation',
                                      'rank',
                                      'surname',
                                      'initials',
                                      'name',
                                      'who_changed',
                                      'when_changed',
                                      'valid',
                                      'orcid',
                                      'scopus_author_id',
                                      'researcherid',
                                      'md5']

EZOP_STG_COLUMNS_DTYPES_TCITATION_AUTHORS = {'citation_author_bk': 'int64',
                                              'citation': 'int64',
                                              'rank': 'int32',
                                              'surname': 'object',
                                              'initials': 'object',
                                              'name': 'object',
                                              'who_changed': 'int32',
                                              'when_changed': 'datetime64[ns]',
                                              'valid': 'object',
                                              'orcid': 'object',
                                              'scopus_author_id': 'object',
                                              'researcherid': 'object'}

#==============================TORGANIZATIONS===================================

EZOP_SRC_TABLE_TORGANIZATIONS = 'TORGANIZATIONS'
EZOP_SRC_COLUMNS_TORGANIZATIONS = ['ORGANIZATION',
                                      'SUPERIOR_ORGANIZATION',
                                      'NESTING_LEVEL',
                                      'ORGANIZATION_TYPE_DOMAIN',
                                      'ORGANIZATION_TYPE_CODE',
                                      'NAME_CS',
                                      'NAME_EN',
                                      'ABBR_CS',
                                      'ABBR_EN',
                                      'CITY_CS',
                                      'CITY_EN',
                                      'LEGAL_STATUS_DOMAIN',
                                      'LEGAL_STATUS_CODE',
                                      'STATE_DOMAIN',
                                      'STATE_CODE',
                                      'WEB',
                                      'REGISTRATION_NUMBER',
                                      'VALID_FROM',
                                      'VALID_TO',
                                      'EDITABLE',
                                      'CODE_MEYS',
                                      'WHO_CHANGED',
                                      'WHEN_CHANGED',
                                      'VALID']

EZOP_STG_TABLE_TORGANIZATIONS = 'torganizations'
EZOP_STG_COLUMNS_TORGANIZATIONS = ['organization_bk',
                                    'superior_organization',
                                    'nesting_level',
                                    'organization_type_domain',
                                    'organization_type_code',
                                    'name_cs',
                                    'name_en',
                                    'abbr_cs',
                                    'abbr_en',
                                    'city_cs',
                                    'city_en',
                                    'legal_status_domain',
                                    'legal_status_code',
                                    'state_domain',
                                    'state_code',
                                    'web',
                                    'registration_number',
                                    'valid_from',
                                    'valid_to',
                                    'editable',
                                    'code_meys',
                                    'who_changed',
                                    'when_changed',
                                    'valid',
                                    'md5']

EZOP_STG_COLUMNS_DTYPES_TORGANIZATIONS = {'organization_bk': 'datetime64[ns]',
                                            'superior_organization': 'int64',
                                            'nesting_level': 'int32',
                                            'organization_type_domain': 'object',
                                            'organization_type_code': 'object',
                                            'name_cs': 'object',
                                            'name_en': 'object',
                                            'abbr_cs': 'object',
                                            'abbr_en': 'object',
                                            'city_cs': 'object',
                                            'city_en': 'object',
                                            'legal_status_domain': 'object',
                                            'legal_status_code': 'object',
                                            'state_domain': 'object',
                                            'state_code': 'object',
                                            'web': 'object',
                                            'registration_number': 'object',
                                            'valid_from': 'datetime64[ns]',
                                            'valid_to': 'datetime64[ns]',
                                            'editable': 'object',
                                            'code_meys': 'object',
                                            'who_changed': 'int64',
                                            'when_changed': 'datetime64[ns]',
                                            'valid': 'object'}


#===============================================================================
#==============================TARGET CONFIGURATIONS============================
#===============================================================================

EZOP_SI_SCHEMA = 'si_ezop'

#================T_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR_REL============

EZOP_SI_TABLE_TCITATION_AFFILIATIONS = 'tcitation_affiliations'
EZOP_SI_COLUMNS_TCITATION_AFFILIATIONS = {'citation_affiliation_bk': 'externiorganizacnijednotka_externicitaceautor_bk',
                                 '\"rank\"': 'poradi',
                                 'affiliation': 'fk_externiorganizacnijednotka',
                                 'citation_author': 'fk_externicitaceautor',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

EZOP_TG_TABLE_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = 't_externiorganizacnijednotka_externicitaceautor_rel'
EZOP_TG_COLUMNS_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = [
                              'externiorganizacnijednotka_externicitaceautor_bk',
                              'poradi',
                              'fk_externiorganizacnijednotka',
                              'fk_externicitaceautor',
                              'version',
                              'date_from',
                              'date_to',
                              'last_update'
    ]
EZOP_TG_COLUMNS_TO_CONCAT_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = [
                              'externiorganizacnijednotka_externicitaceautor_bk',
                              'poradi',
                              'fk_externiorganizacnijednotka',
                              'fk_externicitaceautor'
    ]
EZOP_TG_COLUMNS_DTYPES_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = {
        'externiorganizacnijednotka_externicitaceautor_bk': BigInteger,
        'poradi': Integer,
        'fk_externiorganizacnijednotka': BigInteger,
        'fk_externicitaceautor': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
EZOP_TG_COLUMNS_STRING_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = []
EZOP_TG_COLUMNS_TIMESTAMP_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = []
EZOP_TG_BK_EN_CZ_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR = ('citation_affiliation', 'externiorganizacnijednotka_externicitaceautor_bk') #TODO rename en cz to src tg

#========================T_VVVS_EXTERNI_CITACE_AUTOR============================

EZOP_SI_TABLE_TCITATION_AUTHORS = 'tcitation_authors'
EZOP_SI_COLUMNS_TCITATION_AUTHORS = {'citation': 'fk_citace',
                                 'citation_author_bk': 'externi_citace_autor_bk',
                                 'initials': 'inicialy',
                                 'name': 'jmeno',
                                 'orcid': 'orcid_autor_id',
                                 'rank': 'poradi',
                                 'researcherid': 'wos_autor_id',
                                 'scopus_author_id': 'scopus_autor_id',
                                 'surname': 'prijmeni',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

EZOP_TG_TABLE_EXTERNI_CITACE_AUTOR = 't_vvvs_externi_citace_autor_airflow'
EZOP_TG_COLUMNS_EXTERNI_CITACE_AUTOR = [
        'externi_citace_autor_bk',
        'poradi',
        'prijmeni',
        'inicialy',
        'jmeno',
        'orcid_autor_id',
        'scopus_autor_id',
        'wos_autor_id',
        'fk_citace',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
EZOP_TG_COLUMNS_TO_CONCAT_EXTERNI_CITACE_AUTOR = [
        'externi_citace_autor_bk',
        'poradi',
        'prijmeni',
        'inicialy',
        'jmeno',
        'orcid_autor_id',
        'scopus_autor_id',
        'wos_autor_id',
        'fk_citace'
    ]
EZOP_TG_COLUMNS_DTYPES_EXTERNI_CITACE_AUTOR = {
        'externi_citace_autor_bk': BigInteger,
        'poradi': Integer,
        'prijmeni': VARCHAR(200),
        'inicialy': VARCHAR(80),
        'jmeno': VARCHAR(200),
        'orcid_autor_id': VARCHAR(80),
        'scopus_autor_id': VARCHAR(200),
        'wos_autor_id': VARCHAR(200),
        'fk_citace': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
EZOP_TG_COLUMNS_STRING_EXTERNI_CITACE_AUTOR = ['prijmeni',
                                             'inicialy',
                                             'jmeno',
                                             'orcid_autor_id',
                                             'scopus_autor_id',
                                             'wos_autor_id']
EZOP_TG_COLUMNS_TIMESTAMP_EXTERNI_CITACE_AUTOR = []
EZOP_TG_BK_EN_CZ_EXTERNI_CITACE_AUTOR = ('citation_author', 'externi_citace_autor_bk')

#================T_ORGJ_ORGANIZACNI_JEDNOTKA_EXTERNI============================

EZOP_SI_TABLE_TORGANIZATIONS = 'torganizations'
EZOP_SI_COLUMNS_TORGANIZATIONS = {
                                 'organization_bk': 'externi_organizacni_jednotka_bk',
                                 'superior_organization': 'nadrazena_externi_organizacni_jednotka',
                                 'nesting_level': 'uroven',
                                 'name_cs': 'nazev_cs',
                                 'name_en': 'nazev_en',
                                 'organization_type_code': 'organization_type_code',
                                 'state_code': 'state_code',
                                 'code_meys': 'code_meys',
                                 'valid': 'valid',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

EZOP_TG_TABLE_ORGANIZACNI_JEDNOTKA = 't_orgj_organizacni_jednotka_externi_airflow'
EZOP_TG_COLUMNS_ORGANIZACNI_JEDNOTKA = [
        'externi_organizacni_jednotka_bk',
        'nadrazena_externi_organizacni_jednotka',
        'uroven',
        'nazev_cs',
        'nazev_en',
        'typ_organizace_kod',
        'stat_kod',
        'kod_meys',
        'fk_organizacni_jednotka',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
EZOP_TG_COLUMNS_TO_CONCAT_ORGANIZACNI_JEDNOTKA = [
        'externi_organizacni_jednotka_bk',
        'nadrazena_externi_organizacni_jednotka',
        'uroven',
        'nazev_cs',
        'nazev_en',
        'typ_organizace_kod',
        'stat_kod',
        'kod_meys',
        'fk_organizacni_jednotka'
    ]
EZOP_TG_COLUMNS_DTYPES_ORGANIZACNI_JEDNOTKA = {
        'externi_organizacni_jednotka_bk': BigInteger,
        'nadrazena_externi_organizacni_jednotka': BigInteger,
        'uroven': Integer,
        'nazev_cs': VARCHAR(80),
        'nazev_en': VARCHAR(80),
        'typ_organizace_kod': VARCHAR(20),
        'stat_kod': VARCHAR(20),
        'kod_meys': VARCHAR(5),
        'fk_organizacni_jednotka': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
EZOP_TG_COLUMNS_STRING_ORGANIZACNI_JEDNOTKA = ['nazev_cs',
                                             'nazev_en',
                                             'typ_organizace_kod',
                                             'stat_kod',
                                             'kod_meys']
EZOP_TG_COLUMNS_TIMESTAMP_ORGANIZACNI_JEDNOTKA = []
EZOP_TG_BK_EN_CZ_ORGANIZACNI_JEDNOTKA = ('organization', 'externi_organizacni_jednotka_bk')
