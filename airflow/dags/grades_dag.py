from __future__ import annotations

from datetime import datetime, timedelta

import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.subdag_operator import SubDagOperator


import pandas as pd

import sys
import os

from custom_operators.load_to_stage_operator import LoadToStageOperator
# from custom_operators.load_to_stage_np_operator import LoadToStageNPOperator
from custom_operators.make_increment_operator import MakeIncrementOperator
from custom_operators.load_to_target_operator import LoadToTargetOperator

from custom_operators.helpers.dwh_email import *

from grades.grades_config import *
from target_transformations import process_data_klasifikace
from target_transformations import process_data_klasifikace_student


import logging


DAG_NAME = "grades_dag"
default_args = {
    "owner": "zolockri",
    "depends_on_past": False,
    "start_date": pendulum.datetime(2021, 1, 1, tz="UTC"),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    "execution_timeout": timedelta(minutes=90)
}
with DAG(
    dag_id=DAG_NAME,
    default_args=default_args,
    schedule="0 0 * * *",
    catchup=False,
    # dagrun_timeout=datetime.timedelta(minutes=60)
) as dag:

    task_start_grades = PythonOperator(
        task_id="task_start_grades",
        python_callable=send_email,
        op_kwargs={'subject': 'Airflow: GRADES loading started',
                   'message': f"GRADES loading started at {datetime.now()}"}
    )

#===============================================================================
#================================TO STAGE=======================================
#===============================================================================

#==============TASK LOAD BOOLEAN_STUDENT_CLASSIFICATION=========================

    task_load_boolean_student_classification = LoadToStageOperator(
                task_id="task_load_boolean_student_classification",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_BOOLEAN_STUDENT_CLASSIFICATION,
                src_columns=GRADES_SRC_COLUMNS_BOOLEAN_STUDENT_CLASSIFICATION,
                src_array_size=2*10**5,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_BOOLEAN_STUDENT_CLASSIFICATION,
                stg_columns=GRADES_STG_COLUMNS_BOOLEAN_STUDENT_CLASSIFICATION,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_BOOLEAN_STUDENT_CLASSIFICATION
    )

#================TASK LOAD CLASSIFICATION=======================================

    task_load_classification = LoadToStageOperator(
                task_id="task_load_classification",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_CLASSIFICATION,
                src_columns=GRADES_SRC_COLUMNS_CLASSIFICATION,
                src_array_size=2*10**4,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_CLASSIFICATION,
                stg_columns=GRADES_STG_COLUMNS_CLASSIFICATION,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION
    )
#================TASK LOAD CLASSIFICATION_TEXT==================================

    task_load_classification_text = LoadToStageOperator(
                task_id="task_load_classification_text",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_CLASSIFICATION_TEXT,
                src_columns=GRADES_SRC_COLUMNS_CLASSIFICATION_TEXT,
                src_array_size=5*10**4,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_CLASSIFICATION_TEXT,
                stg_columns=GRADES_STG_COLUMNS_CLASSIFICATION_TEXT,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION_TEXT
    )
#================TASK LOAD CLASSIFICATION_USER==================================

    task_load_classification_user = LoadToStageOperator(
                task_id="task_load_classification_user",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_CLASSIFICATION_USER,
                src_columns=GRADES_SRC_COLUMNS_CLASSIFICATION_USER,
                src_array_size=10**4,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_CLASSIFICATION_USER,
                stg_columns=GRADES_STG_COLUMNS_CLASSIFICATION_USER,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION_USER
    )
#================TASK LOAD NUMBER_STUDENT_CLASSIFICATION========================

    task_load_number_student_classification = LoadToStageOperator(
                task_id="task_load_number_student_classification",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_NUMBER_STUDENT_CLASSIFICATION,
                src_columns=GRADES_SRC_COLUMNS_NUMBER_STUDENT_CLASSIFICATION,
                src_array_size=10**5,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_NUMBER_STUDENT_CLASSIFICATION,
                stg_columns=GRADES_STG_COLUMNS_NUMBER_STUDENT_CLASSIFICATION,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_NUMBER_STUDENT_CLASSIFICATION
    )
#================TASK LOAD STRING_STUDENT_CLASSIFICATION========================

    task_load_string_student_classification = LoadToStageOperator(
                task_id="task_load_string_student_classification",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_STRING_STUDENT_CLASSIFICATION,
                src_columns=GRADES_SRC_COLUMNS_STRING_STUDENT_CLASSIFICATION,
                src_array_size=10**5,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_STRING_STUDENT_CLASSIFICATION,
                stg_columns=GRADES_STG_COLUMNS_STRING_STUDENT_CLASSIFICATION,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_STRING_STUDENT_CLASSIFICATION
    )

#================TASK LOAD STUDENT_CLASSIFICATION===============================

    task_load_student_classification = LoadToStageOperator(
                task_id="task_load_student_classification",
                src_conn_id=GRADES_SRC_CONN_ID,
                src_db=GRADES_SRC_DB,
                src_schema=GRADES_SRC_SCHEMA,
                src_table=GRADES_SRC_TABLE_STUDENT_CLASSIFICATION,
                src_columns=GRADES_SRC_COLUMNS_STUDENT_CLASSIFICATION,
                src_array_size=10**5,
                stg_conn_id=GRADES_STG_CONN_ID,
                stg_db=GRADES_STG_DB,
                stg_schema=GRADES_STG_SCHEMA,
                stg_table=GRADES_STG_TABLE_STUDENT_CLASSIFICATION,
                stg_columns=GRADES_STG_COLUMNS_STUDENT_CLASSIFICATION,
                stg_columns_dtype=GRADES_STG_COLUMNS_DTYPES_STUDENT_CLASSIFICATION
    )

# ===============================================================================
# ================================INCREMENT======================================
# ===============================================================================

# ================TASK MAKE INCREMENT CLASSIFICATION=============================

    task_make_increment_classification = MakeIncrementOperator(
        task_id="task_make_increment_classification",
        source_name=GRADES_SRC_NAME,
        table_name=GRADES_STG_TABLE_CLASSIFICATION,
        conn_id=GRADES_STG_CONN_ID,
        db=GRADES_STG_DB,
        is_psc=False
    )

# ================TASK MAKE INCREMENT STUDENT_CLASSIFICATION=====================

    task_make_increment_student_classification = MakeIncrementOperator(
        task_id="task_make_increment_student_classification",
        source_name=GRADES_SRC_NAME,
        table_name=GRADES_STG_TABLE_STUDENT_CLASSIFICATION,
        conn_id=GRADES_STG_CONN_ID,
        db=GRADES_STG_DB,
        is_psc=False
    )

# ===============================================================================
# =================================TARGET========================================
# ===============================================================================

# ====================TASK LOAD T_KLAS_KLASIFIKACE===============================

    task_load_t_klas_klasifikace = LoadToTargetOperator(
        task_id="task_load_t_klas_klasifikace",
        #===conections===
        stg_conn_id=GRADES_STG_CONN_ID,
        stg_db=GRADES_STG_DB,
        tg_conn_id=GRADES_TG_CONN_ID,
        tg_db=GRADES_TG_DB,
        #===table info===
        si_schema=GRADES_SI_SCHEMA,
        si_table=GRADES_SI_TABLE_CLASSIFICATION,
        si_columns=GRADES_SI_COLUMNS_CLASSIFICATION,
        tg_schema=GRADES_TG_SCHEMA,
        tg_table=GRADES_TG_TABLE_KLASIFIKACE,
        tg_columns=GRADES_TG_COLUMNS_KLASIFIKACE,
        #===info for transforamtions===
        columns_to_concat=GRADES_TG_COLUMNS_TO_CONCAT_KLASIFIKACE,
        tg_dtypes=GRADES_TG_COLUMNS_DTYPES_KLASIFIKACE,
        str_columns=GRADES_TG_COLUMNS_STRING_KLASIFIKACE,
        timestamp_columns=GRADES_TG_COLUMNS_TIMESTAMP_KLASIFIKACE,
        init_load=False,
        bk_en_cz=GRADES_TG_BK_EN_CZ_KLASIFIKACE,
        process_data=process_data_klasifikace.process_data
    )

#=================TASK LOAD T_KLAS_KLASIFIKACE_STUDENT==========================

    task_load_t_klas_klasifikace_student = LoadToTargetOperator(
        task_id="task_load_t_klas_klasifikace_student",
        #===conections===
        stg_conn_id=GRADES_STG_CONN_ID,
        stg_db=GRADES_STG_DB,
        tg_conn_id=GRADES_TG_CONN_ID,
        tg_db=GRADES_TG_DB,
        #===table info===
        si_schema=GRADES_SI_SCHEMA,
        si_table=GRADES_SI_TABLE_CLASSIFICATION_STUDENT,
        si_columns=GRADES_SI_COLUMNS_CLASSIFICATION_STUDENT,
        tg_schema=GRADES_TG_SCHEMA,
        tg_table=GRADES_TG_TABLE_KLASIFIKACE_STUDENT,
        tg_columns=GRADES_TG_COLUMNS_KLASIFIKACE_STUDENT,
        #===info for transforamtions===
        columns_to_concat=GRADES_TG_COLUMNS_TO_CONCAT_KLASIFIKACE_STUDENT,
        tg_dtypes=GRADES_TG_COLUMNS_DTYPES_KLASIFIKACE_STUDENT,
        str_columns=GRADES_TG_COLUMNS_STRING_KLASIFIKACE_STUDENT,
        timestamp_columns=GRADES_TG_COLUMNS_TIMESTAMP_KLASIFIKACE_STUDENT,
        init_load=False,
        bk_en_cz=GRADES_TG_BK_EN_CZ_KLASIFIKACE_STUDENT,
        process_data=process_data_klasifikace_student.process_data
    )

#===============================================================================
    task_start_grades >> [task_load_boolean_student_classification,
                          task_load_classification,
                          task_load_classification_text,
                          task_load_student_classification,
                          task_load_classification_user,
                          task_load_number_student_classification,
                          task_load_string_student_classification
                          ]
    task_load_classification >> task_make_increment_classification
    task_load_student_classification >> task_make_increment_student_classification
    [task_make_increment_student_classification,
    task_load_boolean_student_classification,
    task_load_number_student_classification,
    task_load_string_student_classification,
    task_load_classification_user] >> task_load_t_klas_klasifikace_student
    [task_make_increment_classification,
    task_load_classification_text] >> task_load_t_klas_klasifikace

# if __name__ == "__main__":
#     dag.test()
