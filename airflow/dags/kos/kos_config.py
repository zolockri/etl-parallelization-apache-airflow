from sqlalchemy.sql.sqltypes import *

#============================CONNECTIONS INFO===================================
KOS_SRC_CONN_ID = KOS_SRC_NAME = 'kos'
KOS_SRC_DB = None
KOS_SRC_SCHEMA = 'ST'

KOS_STG_CONN_ID = KOS_TG_CONN_ID = 'dv5'
KOS_STG_DB = 'dwh3_stage_kika'
KOS_STG_SCHEMA = 'ps_kos'

KOS_TG_DB = 'dwh3_target_kika'
KOS_TG_SCHEMA = 'dwh'

#===============================================================================
#===============================STAGE CONFIGURATIONS============================
#===============================================================================

#==================================TUSERS=======================================

KOS_SRC_TABLE_TUSERS = 'TUSERS'
KOS_SRC_COLUMNS_TUSERS = ['ID',
                                    'USERNAME',
                                    'OSOBA_ID']

KOS_STG_TABLE_TUSERS = 'tusers_airflow'
KOS_STG_COLUMNS_TUSERS = ['id',
                                  'username',
                                  'osoba_id',
                                  'md5']

KOS_STG_COLUMNS_DTYPES_TUSERS = {'id': 'int64',
                                  'username': 'object',
                                  'osoba_id': 'int64'}

#================================TKONTAKT=======================================

KOS_SRC_TABLE_TKONTAKT = 'TKONTAKT'
KOS_SRC_COLUMNS_TKONTAKT = [
                                       'ULICE',
                                       'MISTO',
                                       'POSTA',
                                       'PSC',
                                       'ZEME',
                                       'TELEFON',
                                       'FAX',
                                       'EMAIL',
                                       'KDOZAP',
                                       'KDYZAP',
                                       'DEL',
                                       'PLATIOD',
                                       'PLATIDO',
                                       'KOD_OBCE',
                                       'KOD_CASTI_OBCE',
                                       'CISLO',
                                       'OKRES',
                                       'POMRAD',
                                       'TYP',
                                       'STUD_ID',
                                       'OSOBA_ID',
                                       'KONIDNO',
                                       'FIX',
                                       'EMKOD',
                                       'KOREKTNI',
                                       'OVERENO',
                                       'OVERENO_KDY',
                                       'OVERENO_KYM']

KOS_STG_TABLE_TKONTAKT = 'tkontakt_airflow'
KOS_STG_COLUMNS_TKONTAKT = ['ulice',
                                       'misto',
                                       'posta',
                                       'psc',
                                       'zeme',
                                       'telefon',
                                       'fax',
                                       'email',
                                       'kdozap',
                                       'kdyzap',
                                       'del',
                                       'platiod',
                                       'platido',
                                       'kod_obce',
                                       'kod_casti_obce',
                                       'cislo',
                                       'okres',
                                       'pomrad',
                                       'typ',
                                       'stud_id',
                                       'osoba_id',
                                       'konidno',
                                       'fix',
                                       'emkod',
                                       'korektni',
                                       'overeno',
                                       'overeno_kdy',
                                       'overeno_kym',
                                       'md5']

KOS_STG_COLUMNS_DTYPES_TKONTAKT = {'ulice': 'object',
                                   'misto': 'object',
                                   'posta': 'object',
                                   'psc': 'object',
                                   'zeme': 'object',
                                   'telefon': 'object',
                                   'fax': 'object',
                                   'email': 'object',
                                   'kdozap': 'object',
                                   'kdyzap': 'datetime64[ns]',
                                   'del': 'object',
                                   'platiod': 'datetime64[ns]',
                                   'platido': 'datetime64[ns]',
                                   'kod_obce': 'object',
                                   'kod_casti_obce': 'object',
                                   'cislo': 'object',
                                   'okres': 'object',
                                   'pomrad': 'object',
                                   'typ': 'object',
                                   'stud_id': 'int64',
                                   'osoba_id': 'int64',
                                   'konidno': 'int64',
                                   'fix': 'object',
                                   'emkod': 'object',
                                   'korektni': 'object',
                                   'overeno': 'object',
                                   'overeno_kdy': 'datetime64[ns]',
                                   'overeno_kym': 'object'}

#=====================================TEKNS=====================================

KOS_SRC_TABLE_TEKNS = 'TEKNS'
KOS_SRC_COLUMNS_TEKNS = ['NSIDNO',
                                       'EJIDNO',
                                       'TYPSTRIDNO',
                                       'NADRIZ',
                                       'NS',
                                       'NAZEV',
                                       'UCTOVAT',
                                       'KDOZAP',
                                       'KDYZAP',
                                       'KDOSEQ',
                                       'DEL',
                                       'KONIDNO',
                                       'NAZEV_AN',
                                       'NAZEV_L',
                                       'NAZEV_AN_L',
                                       'POPIS',
                                       'PORADI',
                                       'POPIS_AN',
                                       'ZKRATKA',
                                       'ZKRATKA_EN',
                                       'DIVIZE',
                                       'KOD_VE_FIS',
                                       'ZKRATKA_WEB',
                                       'NS_REALNE']

KOS_STG_TABLE_TEKNS = 'tekns_airflow'
KOS_STG_COLUMNS_TEKNS = ['nsidno',
                                       'ejidno',
                                       'typstridno',
                                       'nadriz',
                                       'ns',
                                       'nazev',
                                       'uctovat',
                                       'kdozap',
                                       'kdyzap',
                                       'kdoseq',
                                       'del',
                                       'konidno',
                                       'nazev_an',
                                       'nazev_l',
                                       'nazev_an_l',
                                       'popis',
                                       'poradi',
                                       'popis_an',
                                       'zkratka',
                                       'zkratka_en',
                                       'divize',
                                       'kod_ve_fis',
                                       'zkratka_web',
                                       'ns_realne',
                                       'md5']
KOS_STG_COLUMNS_DTYPES_TEKNS = {'nsidno': 'int64',
                               'ejidno': 'int64',
                               'typstridno': 'int64',
                               'nadriz': 'int64',
                               'ns': 'object',
                               'nazev': 'object',
                               'uctovat': 'object',
                               'kdozap': 'object',
                               'kdyzap': 'datetime64[ns]',
                               'kdoseq': 'object',
                               'del': 'object',
                               'konidno': 'int64',
                               'nazev_an': 'object',
                               'nazev_l': 'object',
                               'nazev_an_l': 'object',
                               'popis': 'int64',
                               'poradi': 'int32',
                               'popis_an': 'int64',
                               'zkratka': 'object',
                               'zkratka_en': 'object',
                               'divize': 'object',
                               'kod_ve_fis': 'object',
                               'zkratka_web': 'object',
                               'ns_realne': 'object'}

#===============================================================================
#==============================TARGET CONFIGURATIONS============================
#===============================================================================

KOS_SI_SCHEMA = 'si_kos'

#==================================T_KOUD_ADRESA================================

KOS_SI_TABLE_TKONTAKT_ADRESA = 'tkontakt_airflow'
KOS_SI_COLUMNS_TKONTAKT_ADRESA = {'ulice': 'ulice',
                                 'psc': 'psc',
                                 'zeme': 'kod_zeme',
                                 'kod_obce': 'kod_obce',
                                 'kod_casti_obce': 'kod_casti_obce',
                                 'cislo': 'cislo_popisne',
                                 'okres': 'kod_okresu',
                                 'typ': 'adresa_typ_bk',
                                 'osoba_id': 'fk_osoba_peridno_bk',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

KOS_TG_TABLE_KOUD_ADRESA = 't_koud_adresa_airflow'
KOS_TG_COLUMNS_KOUD_ADRESA = ['adresa_typ_bk',
                              'ulice',
                              'cislo_popisne',
                              'kod_casti_obce',
                              'kod_obce',
                              'kod_okresu',
                              'kod_zeme',
                              'psc',
                              'fk_osoba_peridno_bk',
                              'version',
                              'date_from',
                              'date_to',
                              'last_update'
    ]
KOS_TG_COLUMNS_TO_CONCAT_KOUD_ADRESA = ['adresa_typ_bk',
                              'fk_osoba_peridno_bk',
                              'ulice',
                              'cislo_popisne',
                              'kod_casti_obce',
                              'kod_obce',
                              'kod_okresu',
                              'kod_zeme',
                              'psc'
    ]
KOS_TG_COLUMNS_DTYPES_KOUD_ADRESA = {
        'adresa_typ_bk': VARCHAR(5),
        'ulice': VARCHAR(50),
        'cislo_popisne': VARCHAR(10),
        'kod_casti_obce': VARCHAR(6),
        'kod_obce': VARCHAR(6),
        'kod_okresu': VARCHAR(20),
        'kod_zeme': VARCHAR(4),
        'psc': VARCHAR(20),
        'fk_osoba_peridno_bk': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
KOS_TG_COLUMNS_STRING_KOUD_ADRESA = ['adresa_typ_bk',
                                      'ulice',
                                      'cislo_popisne',
                                      'kod_casti_obce',
                                      'kod_obce',
                                      'kod_okresu',
                                      'kod_zeme',
                                      'psc'
    ]
KOS_TG_COLUMNS_TIMESTAMP_KOUD_ADRESA = []
KOS_TG_BK_EN_CZ_KOUD_ADRESA = ('typ', 'adresa_typ_bk') #TODO rename en cz to src tg

#===========================T_KOUD_TELEFONNI_CISLO==============================

KOS_SI_TABLE_TKONTAKT_TELEFONNI_CISLO = KOS_SI_TABLE_TKONTAKT_ADRESA
KOS_SI_COLUMNS_TKONTAKT_TELEFONNI_CISLO = {'telefon': 'telefonni_cislo',
                                 'fax': 'cislo_fax',
                                 'typ': 'telefonni_cislo_typ_bk',
                                 'osoba_id': 'fk_osoba_peridno_bk',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

KOS_TG_TABLE_KOUD_TELEFONNI_CISLO = 't_koud_telefonni_cislo_airflow'
KOS_TG_COLUMNS_KOUD_TELEFONNI_CISLO = [
        'telefonni_cislo_typ_bk',
        'telefonni_cislo',
        'cislo_fax',
        'fk_osoba_peridno_bk',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
KOS_TG_COLUMNS_TO_CONCAT_KOUD_TELEFONNI_CISLO = [
        'telefonni_cislo_typ_bk',
        'fk_osoba_peridno_bk',
        'telefonni_cislo',
        'cislo_fax'
    ]
KOS_TG_COLUMNS_DTYPES_KOUD_TELEFONNI_CISLO = {
        'telefonni_cislo_typ_bk': VARCHAR(5),
        'telefonni_cislo': BigInteger,
        'telefonni_cislo': VARCHAR(50),
        'cislo_fax': VARCHAR(50),
        'fk_osoba_peridno_bk': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
KOS_TG_COLUMNS_STRING_KOUD_TELEFONNI_CISLO = ['telefonni_cislo_typ_bk',
                                              'telefonni_cislo',
                                              'cislo_fax']
KOS_TG_COLUMNS_TIMESTAMP_KOUD_TELEFONNI_CISLO = []
KOS_TG_BK_EN_CZ_KOUD_TELEFONNI_CISLO = ('typ', 'telefonni_cislo_typ_bk')


#=================================T_KOUD_EMAIL==================================

KOS_SI_TABLE_TKONTAKT_EMAIL = KOS_SI_TABLE_TKONTAKT_ADRESA
KOS_SI_COLUMNS_TKONTAKT_EMAIL = {'email': 'email_adresa',
                                 'typ': 'email_typ_bk',
                                 'osoba_id': 'fk_osoba_peridno_bk',
                                 'active': 'active',
                                 'last_update': 'last_update',
                                 '\"state\"': 'state'
}

KOS_TG_TABLE_KOUD_EMAIL = 't_koud_email_airflow'
KOS_TG_COLUMNS_KOUD_EMAIL = [
        'email_typ_bk',
        'email_adresa',
        'fk_osoba_peridno_bk',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
KOS_TG_COLUMNS_TO_CONCAT_KOUD_EMAIL = [
        'email_typ_bk',
        'fk_osoba_peridno_bk',
        'email_adresa'
    ]
KOS_TG_COLUMNS_DTYPES_KOUD_EMAIL = {
        'email_typ_bk': VARCHAR(5),
        'email_adresa': VARCHAR(100),
        'fk_osoba_peridno_bk': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
KOS_TG_COLUMNS_STRING_KOUD_EMAIL = []
KOS_TG_COLUMNS_TIMESTAMP_KOUD_EMAIL = []
KOS_TG_BK_EN_CZ_KOUD_EMAIL = ('typ', 'email_typ_bk')


#==========================T_ORG_ORGANIZACNI_JEDNOTKA===========================

KOS_SI_TABLE_TEKNS = 'tekns_airflow'
KOS_SI_COLUMNS_TEKNS = {'nsidno': 'organizacni_jednotka_bk',
                        'ejidno': 'cislo_ekonomicke_jednotky',
                        'typstridno': 'priznak_jednotky',
                        'nadriz': 'fk_organizacnijednotka_nadrizena',
                        'ns': 'cislo_utvaru_id',
                        'nazev': 'nazev_kratky_cs',
                        'nazev_an': 'nazev_kratky_en',
                        'nazev_l': 'nazev_cs',
                        'nazev_an_l': 'nazev_en',
                        'zkratka': 'zkratka_cs',
                        'zkratka_en': 'zkratka_en',
                        'active': 'active',
                        'last_update': 'last_update',
                        '\"state\"': 'state'
}

KOS_TG_TABLE_ORGANIZACNI_JEDNOTKA = 't_orgj_organizacni_jednotka_airflow'
KOS_TG_COLUMNS_ORGANIZACNI_JEDNOTKA = [
        'organizacni_jednotka_bk',
        'priznak_jednotky',
        'cislo_utvaru_id',
        'cislo_ekonomicke_jednotky',
        'nazev_cs',
        'nazev_en',
        'nazev_kratky_cs',
        'nazev_kratky_en',
        'zkratka_cs',
        'zkratka_en',
        'fk_organizacnijednotka_nadrizena',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
KOS_TG_COLUMNS_TO_CONCAT_ORGANIZACNI_JEDNOTKA = [
        'organizacni_jednotka_bk',
        'priznak_jednotky',
        'cislo_utvaru_id',
        'cislo_ekonomicke_jednotky',
        'nazev_cs',
        'nazev_en',
        'nazev_kratky_cs',
        'nazev_kratky_en',
        'zkratka_cs',
        'zkratka_en',
        'fk_organizacnijednotka_nadrizena'
    ]
KOS_TG_COLUMNS_DTYPES_ORGANIZACNI_JEDNOTKA = {
        'organizacni_jednotka_bk': BigInteger,
        'priznak_jednotky': Integer,
        'cislo_utvaru_id': VARCHAR(15),
        'cislo_ekonomicke_jednotky': BigInteger,
        'nazev_cs': VARCHAR(200),
        'nazev_en': VARCHAR(200),
        'nazev_kratky_cs': VARCHAR(35),
        'nazev_kratky_en': VARCHAR(35),
        'zkratka_cs': VARCHAR(5),
        'zkratka_en': VARCHAR(5),
        'fk_organizacnijednotka_nadrizena': BigInteger,
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
KOS_TG_COLUMNS_STRING_ORGANIZACNI_JEDNOTKA = [
                'cislo_utvaru_id',
                'nazev_cs',
                'nazev_en',
                'nazev_kratky_cs',
                'nazev_kratky_en',
                'zkratka_cs',
                'zkratka_en'
]
KOS_TG_COLUMNS_TIMESTAMP_ORGANIZACNI_JEDNOTKA = []
KOS_TG_BK_EN_CZ_ORGANIZACNI_JEDNOTKA = ('nsidno', 'organizacni_jednotka_bk')
