from sqlalchemy.sql.sqltypes import *
import numpy as np
import pandas as pd

#============================CONNECTIONS INFO===================================
USERMAP_SRC_CONN_ID = USERMAP_SRC_NAME = 'usermap'
USERMAP_SRC_DB = None
USERMAP_SRC_SCHEMA = 'EXT_DS'

USERMAP_STG_CONN_ID = USERMAP_TG_CONN_ID = 'dv5'
USERMAP_STG_DB = 'dwh3_stage_kika'
USERMAP_STG_SCHEMA = 'ps_usermap'

USERMAP_TG_DB = 'dwh3_target_kika'
USERMAP_TG_SCHEMA = 'dwh'

#===============================================================================
#===============================STAGE CONFIGURATIONS============================
#===============================================================================

#==================================OSOBY========================================

USERMAP_SRC_TABLE_OSOBY = 'OSOBY'
USERMAP_SRC_COLUMNS_OSOBY = ['OSOBA',
                                    'UZIVATELSKE_JMENO',
                                    'KVALITA_IDENTITY_DOMENA',
                                    'KVALITA_IDENTITY_KOD',
                                    'KVALITA_IDENTITY',
                                    'RODNE_CISLO_STATNI',
                                    'RODNE_CISLO_MSMT',
                                    'JMENO',
                                    'PRIJMENI',
                                    'RODNE_PRIJMENI',
                                    'CELE_JMENO',
                                    'TITULY_PRED_DOMENA',
                                    'TITULY_PRED_KOD',
                                    'TITULY_PRED',
                                    'TITULY_ZA_DOMENA',
                                    'TITULY_ZA_KOD',
                                    'TITULY_ZA',
                                    'POHLAVI_DOMENA',
                                    'POHLAVI_KOD',
                                    'POHLAVI',
                                    'DATUM_NAROZENI',
                                    'STATNI_PRISLUSNOST',
                                    'STATNI_PRISLUSNOST_N3',
                                    'STATNI_PRISLUSNOST_A2',
                                    'STATNI_PRISLUSNOST_A3',
                                    'STATNI_PRISLUSNOST_NAZEV',
                                    'STAT_NAROZENI',
                                    'STAT_NAROZENI_N3',
                                    'STAT_NAROZENI_A2',
                                    'STAT_NAROZENI_A3',
                                    'STAT_NAROZENI_NAZEV',
                                    'MISTO_NAROZENI',
                                    'DOKLAD_TOTOZNOSTI_DOMENA',
                                    'DOKLAD_TOTOZNOSTI_KOD',
                                    'DOKLAD_TOTOZNOSTI',
                                    'DOKLAD_TOTOZNOSTI_CISLO',
                                    'KDO_VLOZIL',
                                    'KDY_VLOZIL',
                                    'KDO_ZAPSAL',
                                    'KDY_ZAPSAL',
                                    'DUVOD_ZNEPLATNENI']

USERMAP_STG_TABLE_OSOBY = 'osoby_airflow'
USERMAP_STG_COLUMNS_OSOBY = ['osoba',
                             'uzivatelske_jmeno',
                             'kvalita_identity_domena',
                             'kvalita_identity_kod',
                             'kvalita_identity',
                             'rodne_cislo_statni',
                             'rodne_cislo_msmt',
                             'jmeno',
                             'prijmeni',
                             'rodne_prijmeni',
                             'cele_jmeno',
                             'tituly_pred_domena',
                             'tituly_pred_kod',
                             'tituly_pred',
                             'tituly_za_domena',
                             'tituly_za_kod',
                             'tituly_za',
                             'pohlavi_domena',
                             'pohlavi_kod',
                             'pohlavi',
                             'datum_narozeni',
                             'statni_prislusnost',
                             'statni_prislusnost_n3',
                             'statni_prislusnost_a2',
                             'statni_prislusnost_a3',
                             'statni_prislusnost_nazev',
                             'stat_narozeni',
                             'stat_narozeni_n3',
                             'stat_narozeni_a2',
                             'stat_narozeni_a3',
                             'stat_narozeni_nazev',
                             'misto_narozeni',
                             'doklad_totoznosti_domena',
                             'doklad_totoznosti_kod',
                             'doklad_totoznosti',
                             'doklad_totoznosti_cislo',
                             'kdo_vlozil',
                             'kdy_vlozil',
                             'kdo_zapsal',
                             'kdy_zapsal',
                             'duvod_zneplatneni',
                             'md5']
USERMAP_STG_COLUMNS_DTYPES_OSOBA = {
        'osoba': 'int64',
         'uzivatelske_jmeno': 'object',
         'kvalita_identity_domena': 'object',
         'kvalita_identity_kod': 'object',
         'kvalita_identity': 'object',
         'rodne_cislo_statni': 'object',
         'rodne_cislo_msmt': 'object',
         'jmeno': 'object',
         'prijmeni': 'object',
         'rodne_prijmeni': 'object',
         'cele_jmeno': 'object',
         'tituly_pred_domena': 'object',
         'tituly_pred_kod': 'object',
         'tituly_pred': 'object',
         'tituly_za_domena': 'object',
         'tituly_za_kod': 'object',
         'tituly_za': 'object',
         'pohlavi_domena': 'object',
         'pohlavi_kod': 'object',
         'pohlavi': 'object',
         'datum_narozeni': 'datetime64[ns]',
         'statni_prislusnost': 'int32', # can be NULL
         'statni_prislusnost_n3': 'object',
         'statni_prislusnost_a2': 'object',
         'statni_prislusnost_a3': 'object',
         'statni_prislusnost_nazev': 'object',
         'stat_narozeni': 'int32', # can be NULL
         'stat_narozeni_n3': 'object',
         'stat_narozeni_a2': 'object',
         'stat_narozeni_a3': 'object',
         'stat_narozeni_nazev': 'object',
         'misto_narozeni': 'object',
         'doklad_totoznosti_domena': 'object',
         'doklad_totoznosti_kod': 'object',
         'doklad_totoznosti': 'object',
         'doklad_totoznosti_cislo': 'object',
         'kdo_vlozil': 'int32',
         'kdy_vlozil': 'datetime64[ns]',
         'kdo_zapsal': 'int32',
         'kdy_zapsal': 'datetime64[ns]',
         'duvod_zneplatneni': 'int32' # can be NULL
         # 'md5': 'object'
}


#===============================================================================
#==============================TARGET CONFIGURATIONS============================
#===============================================================================

USERMAP_SI_SCHEMA = 'si_usermap'

#===================================T_OSOB_OSOBA================================

USERMAP_SI_TABLE_OSOBA = 'osoby_airflow'
USERMAP_SI_COLUMNS_OSOBA = { 'osoba': 'osoba_bk',
                             'uzivatelske_jmeno': 'uzivatelske_jmeno',
                             'kvalita_identity_domena': 'kvalita_identity_domena',
                             'kvalita_identity_kod': 'kvalita_identity_kod',
                             'kvalita_identity': 'kvalita_identity',
                             'rodne_cislo_statni': 'rodne_cislo_statni',
                             'rodne_cislo_msmt': 'rodne_cislo_msmt',
                             'jmeno': 'jmeno',
                             'prijmeni': 'prijmeni',
                             'rodne_prijmeni': 'rodne_prijmeni',
                             'cele_jmeno': 'cele_jmeno',
                             'tituly_pred_domena': 'tituly_pred_domena',
                             'tituly_pred_kod': 'tituly_pred_kod',
                             'tituly_pred': 'tituly_pred',
                             'tituly_za': 'tituly_za',
                             'tituly_za_kod': 'tituly_za_kod',
                             'tituly_za': 'tituly_za',
                             'pohlavi_domena': 'pohlavi_domena',
                             'pohlavi_kod': 'pohlavi_kod',
                             'pohlavi': 'pohlavi',
                             'datum_narozeni': 'datum_narozeni',
                             'statni_prislusnost': 'statni_prislusnost',
                             'statni_prislusnost_n3': 'statni_prislusnost_n3',
                             'statni_prislusnost_a2': 'statni_prislusnost_a2',
                             'statni_prislusnost_a3': 'statni_prislusnost_a3',
                             'statni_prislusnost_nazev': 'statni_prislusnost_nazev',
                             'stat_narozeni': 'stat_narozeni',
                             'stat_narozeni_n3': 'stat_narozeni_n3',
                             'stat_narozeni_a2': 'stat_narozeni_a2',
                             'stat_narozeni_a3': 'stat_narozeni_a3',
                             'stat_narozeni_nazev': 'stat_narozeni_nazev',
                             'misto_narozeni': 'misto_narozeni',
                             'doklad_totoznosti_domena': 'doklad_totoznosti_domena',
                             'doklad_totoznosti_kod': 'doklad_totoznosti_kod',
                             'doklad_totoznosti': 'doklad_totoznosti',
                             'doklad_totoznosti_cislo': 'doklad_totoznosti_cislo',
                             'kdo_vlozil': 'kdo_vlozil',
                             'kdy_vlozil': 'kdy_vlozil',
                             'kdo_zapsal': 'kdo_zapsal',
                             'kdy_zapsal': 'kdy_zapsal',
                             'duvod_zneplatneni': 'duvod_zneplatneni',
                             'active': 'active',
                             'last_update': 'last_update',
                             '\"state\"': 'state'
}

USERMAP_TG_TABLE_OSOBA = 't_osob_osoba_airflow'
USERMAP_TG_COLUMNS_OSOBA = ['osoba_bk',
                             'kos_osoba_peridno',
                             'uzivatelske_jmeno',
                             'tituly_pred',
                             'jmeno',
                             'prijmeni',
                             'tituly_za',
                             'cele_jmeno',
                             'rodne_prijmeni',
                             'rodne_cislo_statni',
                             'rodne_cislo_msmt',
                             'pohlavi_kod',
                             'datum_narozeni',
                             'misto_narozeni',
                             'statni_prislusnost_nazev',
                             'stat_narozeni_nazev',
                             'doklad_totoznosti_kod',
                             'doklad_totoznosti_cislo',
                              'version',
                              'date_from',
                              'date_to',
                              'last_update'
    ]
USERMAP_TG_COLUMNS_TO_CONCAT_OSOBA = ['osoba_bk',
                             'kos_osoba_peridno',
                             'uzivatelske_jmeno',
                             'tituly_pred',
                             'jmeno',
                             'prijmeni',
                             'tituly_za',
                             'cele_jmeno',
                             'rodne_prijmeni',
                             'rodne_cislo_statni',
                             'rodne_cislo_msmt',
                             'pohlavi_kod',
                             'datum_narozeni',
                             'misto_narozeni',
                             'statni_prislusnost_nazev',
                             'stat_narozeni_nazev',
                             'doklad_totoznosti_kod',
                             'doklad_totoznosti_cislo'
    ]
USERMAP_TG_COLUMNS_DTYPES_OSOBA = {
        'osoba_bk': BigInteger,
        'kos_osoba_peridno': BigInteger,
        'uzivatelske_jmeno': VARCHAR(20),
        'tituly_pred': TEXT,
        'jmeno': VARCHAR(24),
        'prijmeni': VARCHAR(40),
        'tituly_za': TEXT,
        'cele_jmeno': TEXT,
        'rodne_prijmeni': VARCHAR(40),
        'rodne_cislo_statni': VARCHAR(10),
        'rodne_cislo_msmt': VARCHAR(10),
        'pohlavi_kod': VARCHAR(30),
        'datum_narozeni': Date,
        'misto_narozeni': VARCHAR(100),
        'statni_prislusnost_nazev': VARCHAR(200),
        'stat_narozeni_nazev': VARCHAR(200),
        'doklad_totoznosti_kod': VARCHAR(30),
        'doklad_totoznosti_cislo': VARCHAR(40),
        'version': BigInteger,
        'date_from': TIMESTAMP,
        'date_to': TIMESTAMP,
        'last_update': TIMESTAMP
}
USERMAP_TG_COLUMNS_STRING_OSOBA = [
                             'uzivatelske_jmeno',
                             'tituly_pred',
                             'jmeno',
                             'prijmeni',
                             'tituly_za',
                             'cele_jmeno',
                             'rodne_prijmeni',
                             'rodne_cislo_statni',
                             'rodne_cislo_msmt',
                             'pohlavi_kod',
                             'misto_narozeni',
                             'statni_prislusnost_nazev',
                             'stat_narozeni_nazev',
                             'doklad_totoznosti_kod',
                             'doklad_totoznosti_cislo'
    ]
USERMAP_TG_COLUMNS_TIMESTAMP_OSOBA = ['datum_narozeni']
USERMAP_TG_BK_EN_CZ_OSOBA = ('osoba', 'osoba_bk') #TODO rename en cz to src tg
