from sqlalchemy.sql.sqltypes import *

#============================CONNECTIONS INFO===================================
GRADES_SRC_CONN_ID = GRADES_SRC_NAME = 'grades'
GRADES_SRC_DB = 'classification'
GRADES_SRC_SCHEMA = 'main'

GRADES_STG_CONN_ID = GRADES_TG_CONN_ID = 'dv5'
GRADES_STG_DB = 'dwh3_stage_kika'
GRADES_STG_SCHEMA = 'ps_grades'

GRADES_TG_DB = 'dwh3_target_kika'
GRADES_TG_SCHEMA = 'dwh'

#===============================================================================
#===============================STAGE CONFIGURATIONS============================
#===============================================================================

#=====================BOOLEAN_STUDENT_CLASSIFICATION============================

GRADES_SRC_TABLE_BOOLEAN_STUDENT_CLASSIFICATION = 'boolean_student_classification'
GRADES_SRC_COLUMNS_BOOLEAN_STUDENT_CLASSIFICATION = ['student_classification_id', 'value']

GRADES_STG_TABLE_BOOLEAN_STUDENT_CLASSIFICATION = 'boolean_student_classification_airflow'
GRADES_STG_COLUMNS_BOOLEAN_STUDENT_CLASSIFICATION = ['student_classification_id', 'value', 'md5']

GRADES_STG_COLUMNS_DTYPES_BOOLEAN_STUDENT_CLASSIFICATION = {'student_classification_id': 'int64',
                                                            'value': 'bool'}

#==========================CLASSIFICATION=======================================

GRADES_SRC_TABLE_CLASSIFICATION = 'classification'
GRADES_SRC_COLUMNS_CLASSIFICATION = ['classification_id',
                                    'identifier',
                                    'course_code',
                                    'semester_code',
                                    'classification_type',
                                    'expression',
                                    'value_type',
                                    'minimum_required_value',
                                    'maximum_value',
                                    'hidden',
                                    'index',
                                    'mandatory',
                                    'parent_id',
                                    'evaluation_type',
                                    'aggregated_classification_id',
                                    'aggregation_function',
                                    'aggregation_scope',
                                    'minimum_value']

GRADES_STG_TABLE_CLASSIFICATION = 'classification_airflow'
GRADES_STG_COLUMNS_CLASSIFICATION = ['classification_id',
                                    'identifier',
                                    'course_code',
                                    'semester_code',
                                    'classification_type',
                                    'expression',
                                    'value_type',
                                    'minimum_required_value',
                                    'maximum_value',
                                    'hidden',
                                    'index',
                                    'mandatory',
                                    'parent_id',
                                    'evaluation_type',
                                    'aggregated_classification_id',
                                    'aggregation_function',
                                    'aggregation_scope',
                                    'minimum_value',
                                    'md5']

GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION = {'classification_id': 'int64',
                                            'identifier': 'object',
                                            'course_code': 'object',
                                            'semester_code': 'object',
                                            'classification_type': 'object',
                                            'expression': 'object',
                                            'value_type': 'object',
                                            'minimum_required_value': 'float32',
                                            'maximum_value': 'float32',
                                            'hidden': 'bool',
                                            'index': 'int64',
                                            'mandatory': 'bool',
                                            'parent_id': 'int64',
                                            'evaluation_type': 'object',
                                            'aggregated_classification_id': 'int64',
                                            'aggregation_function': 'object',
                                            'aggregation_scope': 'object',
                                            'minimum_value': 'float32'}

#==========================CLASSIFICATION_TEXT==================================

GRADES_SRC_TABLE_CLASSIFICATION_TEXT = 'classification_text'
GRADES_SRC_COLUMNS_CLASSIFICATION_TEXT = ['language_tag',
                                          'classification_text_id',
                                          'classification_id',
                                          'name']

GRADES_STG_TABLE_CLASSIFICATION_TEXT = 'classification_text_airflow'
GRADES_STG_COLUMNS_CLASSIFICATION_TEXT = ['language_tag',
                                          'classification_text_id',
                                          'classification_id',
                                          'name',
                                          'md5']

GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION_TEXT = {'language_tag': 'object',
                                                  'classification_text_id': 'int64',
                                                  'classification_id': 'int64',
                                                  'name': 'object'}

#=========================CLASSIFICATION_USER===================================

GRADES_SRC_TABLE_CLASSIFICATION_USER = 'classification_user'
GRADES_SRC_COLUMNS_CLASSIFICATION_USER = ['username',
                                         'first_name',
                                         'last_name',
                                         'email',
                                         'full_name',
                                         'personal_number',
                                         'classification_user_id']

GRADES_STG_TABLE_CLASSIFICATION_USER = 'classification_user_airflow'
GRADES_STG_COLUMNS_CLASSIFICATION_USER = ['username',
                                        'first_name',
                                        'last_name',
                                        'email',
                                        'full_name',
                                        'personal_number',
                                        'classification_user_id',
                                        'md5']

GRADES_STG_COLUMNS_DTYPES_CLASSIFICATION_USER = {'username': 'object',
                                                'first_name': 'object',
                                                'last_name': 'object',
                                                'email': 'object',
                                                'full_name': 'object',
                                                'personal_number': 'object',
                                                'classification_user_id': 'int64'}

#======================NUMBER_STUDENT_CLASSIFICATION============================

GRADES_SRC_TABLE_NUMBER_STUDENT_CLASSIFICATION = 'number_student_classification'
GRADES_SRC_COLUMNS_NUMBER_STUDENT_CLASSIFICATION = ['student_classification_id', 'value']

GRADES_STG_TABLE_NUMBER_STUDENT_CLASSIFICATION = 'number_student_classification_airflow'
GRADES_STG_COLUMNS_NUMBER_STUDENT_CLASSIFICATION = ['student_classification_id', 'value', 'md5']

GRADES_STG_COLUMNS_DTYPES_NUMBER_STUDENT_CLASSIFICATION = {'student_classification_id': 'int64',
                                                    'value': 'float32'}

#======================STRING_STUDENT_CLASSIFICATION============================

GRADES_SRC_TABLE_STRING_STUDENT_CLASSIFICATION = 'string_student_classification'
GRADES_SRC_COLUMNS_STRING_STUDENT_CLASSIFICATION = ['student_classification_id', 'value']

GRADES_STG_TABLE_STRING_STUDENT_CLASSIFICATION = 'string_student_classification_airflow'
GRADES_STG_COLUMNS_STRING_STUDENT_CLASSIFICATION = ['student_classification_id', 'value', 'md5']

GRADES_STG_COLUMNS_DTYPES_STRING_STUDENT_CLASSIFICATION = {'student_classification_id': 'int64',
                                                            'value': 'object'}

#==========================STUDENT_CLASSIFICATION===============================

GRADES_SRC_TABLE_STUDENT_CLASSIFICATION = 'student_classification'
GRADES_SRC_COLUMNS_STUDENT_CLASSIFICATION = ['student_classification_id',
                                            'note',
                                            'classification_id',
                                            'timestamp',
                                            'classification_user_id']

GRADES_STG_TABLE_STUDENT_CLASSIFICATION = 'student_classification_airflow'
GRADES_STG_COLUMNS_STUDENT_CLASSIFICATION = ['student_classification_id',
                                            'note',
                                            'classification_id',
                                            'timestamp',
                                            'classification_user_id',
                                            'md5']

GRADES_STG_COLUMNS_DTYPES_STUDENT_CLASSIFICATION = {'student_classification_id': 'int64',
                                                    'note': 'object',
                                                    'classification_id': 'int64',
                                                    'timestamp': 'datetime64[ns]',
                                                    'classification_user_id': 'int64'}
#===============================================================================
#==============================TARGET CONFIGURATIONS============================
#===============================================================================

GRADES_SI_SCHEMA = 'si_grades'

#==============================T_KLAS_KLASIFIKACE===============================

GRADES_SI_TABLE_CLASSIFICATION = 'classification_airflow'
GRADES_SI_COLUMNS_CLASSIFICATION = {
        'classification_id': 'klasifikace_bk',
        'identifier': 'identifikator',
        'course_code': 'predmet_kod',
        'semester_code': 'fk_semestr',
        'classification_type': 'typ',
        'expression': 'rovnice_vypoctu',
        'value_type': 'datovy_typ',
        'minimum_value': 'minimalni_hodnota',
        'minimum_required_value': 'minimalni_vyzadovana_hodnota',
        'maximum_value': 'maximalni_hodnota',
        'hidden': 'skryta',
        'index': 'poradi',
        'mandatory': 'povinny',
        'parent_id': 'fk_nadrazena_klasifikace',
        'evaluation_type': 'typ_vyhodnocovani',
        'aggregated_classification_id': 'fk_agregovana_klasifikace',
        'aggregation_function': 'agregovana_funkce',
        'aggregation_scope': 'rozsah_agregace',
        'active': 'active',
        'last_update': 'last_update',
        '\"state\"': 'state'
}

GRADES_TG_TABLE_KLASIFIKACE = 't_klas_klasifikace_airflow'
GRADES_TG_COLUMNS_KLASIFIKACE = [
        'klasifikace_bk',
        'identifikator',
        'nazev_cs',
        'nazev_en',
        'predmet_kod',
        'fk_semestr',
        'typ',
        'rovnice_vypoctu',
        'datovy_typ',
        'minimalni_hodnota',
        'minimalni_vyzadovana_hodnota',
        'maximalni_hodnota',
        'skryta',
        'poradi',
        'povinny',
        'fk_nadrazena_klasifikace',
        'typ_vyhodnocovani',
        'fk_agregovana_klasifikace',
        'agregovana_funkce',
        'rozsah_agregace',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
GRADES_TG_COLUMNS_TO_CONCAT_KLASIFIKACE = [
        'klasifikace_bk',
        'identifikator',
        'nazev_cs',
        'nazev_en',
        'predmet_kod',
        'fk_semestr',
        'typ',
        'rovnice_vypoctu',
        'datovy_typ',
        'minimalni_hodnota',
        'minimalni_vyzadovana_hodnota',
        'maximalni_hodnota',
        'skryta',
        'poradi',
        'povinny',
        'fk_nadrazena_klasifikace',
        'typ_vyhodnocovani',
        'fk_agregovana_klasifikace',
        'agregovana_funkce',
        'rozsah_agregace'
    ]
GRADES_TG_COLUMNS_DTYPES_KLASIFIKACE = {
        'klasifikace_bk': BigInteger,
        'identifikator': TEXT,
        'nazev_cs': TEXT,
        'nazev_en': TEXT,
        'predmet_kod': TEXT,
        'fk_semestr': TEXT,
        'typ': TEXT,
        'rovnice_vypoctu': TEXT,
        'datovy_typ': TEXT,
        'minimalni_hodnota': Float,
        'minimalni_vyzadovana_hodnota': Float,
        'maximalni_hodnota': Float,
        'skryta': VARCHAR(1),
        'poradi': BigInteger,
        'povinny':VARCHAR(1) ,
        'fk_nadrazena_klasifikace': BigInteger,
        'typ_vyhodnocovani': TEXT,
        'fk_agregovana_klasifikace': BigInteger,
        'agregovana_funkce': TEXT,
        'rozsah_agregace': TEXT,
        'last_update': TIMESTAMP,
        'version': BigInteger,
        'date_to': TIMESTAMP,
        'date_from': TIMESTAMP
}
GRADES_TG_COLUMNS_STRING_KLASIFIKACE = [
        'identifikator',
        'nazev_cs',
        'nazev_en',
        'predmet_kod',
        'fk_semestr',
        'typ',
        'rovnice_vypoctu',
        'datovy_typ',
        'skryta',
        'povinny',
        'typ_vyhodnocovani',
        'agregovana_funkce',
        'rozsah_agregace'
    ]
GRADES_TG_COLUMNS_TIMESTAMP_KLASIFIKACE = []
GRADES_TG_BK_EN_CZ_KLASIFIKACE = ('classification_id', 'klasifikace_bk')

#=========================T_KLAS_KLASIFIKACE_STUDENT============================

GRADES_SI_TABLE_CLASSIFICATION_STUDENT = 'student_classification_airflow'
GRADES_SI_COLUMNS_CLASSIFICATION_STUDENT = {
        'student_classification_id': 'klasifikace_student_bk',
        'note': 'poznamka',
        'classification_id': 'fk_klasifikace',
        'timestamp': 'datum_klasifikace',
        'classification_user_id': 'classification_user_id',
        'active': 'active',
        'last_update': 'last_update',
        '\"state\"': 'state'
}

GRADES_TG_TABLE_KLASIFIKACE_STUDENT = 't_klas_klasifikace_student_airflow'
GRADES_TG_COLUMNS_KLASIFIKACE_STUDENT = [
        'klasifikace_student_bk',
        'hodnota',
        'poznamka',
        'fk_klasifikace',
        'datum_klasifikace',
        'fk_student_umap_peridno',
        'version',
        'date_from',
        'date_to',
        'last_update'
    ]
GRADES_TG_COLUMNS_TO_CONCAT_KLASIFIKACE_STUDENT = [
        'klasifikace_student_bk',
        'hodnota',
        'poznamka',
        'fk_klasifikace',
        'datum_klasifikace',
        'fk_student_umap_peridno'
    ]
GRADES_TG_COLUMNS_DTYPES_KLASIFIKACE_STUDENT = {
        'klasifikace_student_bk': BigInteger,
        'hodnota': TEXT,
        'poznamka': TEXT,
        'fk_klasifikace': BigInteger,
        'datum_klasifikace': TIMESTAMP,
        'fk_student_umap_peridno': BigInteger,
        'last_update': TIMESTAMP,
        'version': BigInteger,
        'date_to': TIMESTAMP,
        'date_from': TIMESTAMP
}
GRADES_TG_COLUMNS_STRING_KLASIFIKACE_STUDENT = str_columns = ['hodnota', 'poznamka']
GRADES_TG_COLUMNS_TIMESTAMP_KLASIFIKACE_STUDENT = ['datum_klasifikace']
GRADES_TG_BK_EN_CZ_KLASIFIKACE_STUDENT = ('student_classification_id', 'klasifikace_student_bk')
