from __future__ import annotations

from datetime import datetime, timedelta

import pendulum

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator, BranchPythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.operators.subdag_operator import SubDagOperator


import pandas as pd

import sys
import os

from custom_operators.load_to_stage_operator import LoadToStageOperator
from custom_operators.load_to_stage_operator import LoadToStageOperator
from custom_operators.make_increment_operator import MakeIncrementOperator
from custom_operators.load_to_target_operator import LoadToTargetOperator

from custom_operators.helpers.dwh_email import *

from ezop.ezop_config import *
from target_transformations import process_data_orgj_externi

import logging


def get_last_success_date(task_id, dag_id):
    session = settings.Session()
    last_success_date = (
        session.query(func.max(TaskInstance.execution_date))
        .filter(TaskInstance.task_id == task_id)
        .filter(TaskInstance.dag_id == dag_id)
        .filter(TaskInstance.state == 'success')
        .scalar()
    )
    session.close()
    return last_success_date.date()

def wait_tekns_done(**kwargs):
    while get_last_success_date("task_load_tekns", "kos_dag") != date.today():
        time.sleep(5)
    return

def is_tekns_done(**kwargs):
    if get_last_success_date("task_load_tekns", "kos_dag") != date.today():
        return 'task_trigger_kos'
    else: return 'task_load_t_orgj_organizacni_jednotka_externi'

DAG_NAME = "ezop_dag"
default_args = {
    "owner": "zolockri",
    "depends_on_past": False,
    "start_date": pendulum.datetime(2021, 1, 1, tz="UTC"),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
    "execution_timeout": timedelta(minutes=90)
}
with DAG(
    dag_id=DAG_NAME,
    default_args=default_args,
    schedule="0 0 * * *",
    catchup=False,
    # dagrun_timeout=datetime.timedelta(minutes=60)
) as dag:
    # 1. SUBDAG: Extract data to stage

    # task_start_grades = EmptyOperator(
    #     task_id="start_grades"
    # )

    task_start_ezop = PythonOperator(
        task_id="task_start_ezop",
        python_callable=send_email,
        op_kwargs={'subject': 'Airflow: EZOP loading started',
                   'message': f"EZOP loading started at {datetime.now()}"}
    )

#===============================================================================
#================================TO STAGE=======================================
#===============================================================================

#======================TASK LOAD TCITATION_AFFILIATIONS=========================

    task_load_tcitation_affiliations = LoadToStageOperator(
                task_id="task_load_tcitation_affiliations",
                src_conn_id=EZOP_SRC_CONN_ID,
                src_db=EZOP_SRC_DB,
                src_schema=EZOP_SRC_SCHEMA,
                src_table=EZOP_SRC_TABLE_TCITATION_AFFILIATIONS,
                src_columns=EZOP_SRC_COLUMNS_TCITATION_AFFILIATIONS,
                src_array_size=5*10**5,
                stg_conn_id=EZOP_STG_CONN_ID,
                stg_db=EZOP_STG_DB,
                stg_schema=EZOP_STG_SCHEMA,
                stg_table=EZOP_STG_TABLE_TCITATION_AFFILIATIONS,
                stg_columns=EZOP_STG_COLUMNS_TCITATION_AFFILIATIONS,
                stg_columns_dtype=EZOP_STG_COLUMNS_DTYPES_TCITATION_AFFILIATIONS
    )

#=========================TASK LOAD TCITATION_AUTHORS===========================

    task_load_tcitation_authors = LoadToStageOperator(
                task_id="task_load_tcitation_authors",
                src_conn_id=EZOP_SRC_CONN_ID,
                src_db=EZOP_SRC_DB,
                src_schema=EZOP_SRC_SCHEMA,
                src_table=EZOP_SRC_TABLE_TCITATION_AUTHORS,
                src_columns=EZOP_SRC_COLUMNS_TCITATION_AUTHORS,
                src_array_size=5*10**5,
                stg_conn_id=EZOP_STG_CONN_ID,
                stg_db=EZOP_STG_DB,
                stg_schema=EZOP_STG_SCHEMA,
                stg_table=EZOP_STG_TABLE_TCITATION_AUTHORS,
                stg_columns=EZOP_STG_COLUMNS_TCITATION_AUTHORS,
                stg_columns_dtype=EZOP_STG_COLUMNS_DTYPES_TCITATION_AUTHORS
    )

#===========================TASK LOAD TORGANIZATIONS============================

    task_load_torganizations = LoadToStageOperator(
                task_id="task_load_torganizations",
                src_conn_id=EZOP_SRC_CONN_ID,
                src_db=EZOP_SRC_DB,
                src_schema='VVVS_CORE',
                src_table=EZOP_SRC_TABLE_TORGANIZATIONS,
                src_columns=EZOP_SRC_COLUMNS_TORGANIZATIONS,
                src_array_size=2*10**4,
                stg_conn_id=EZOP_STG_CONN_ID,
                stg_db=EZOP_STG_DB,
                stg_schema=EZOP_STG_SCHEMA,
                stg_table=EZOP_STG_TABLE_TORGANIZATIONS,
                stg_columns=EZOP_STG_COLUMNS_TORGANIZATIONS,
                stg_columns_dtype=EZOP_STG_COLUMNS_DTYPES_TORGANIZATIONS
    )

# ==============================================================================
# ================================INCREMENT=====================================
# ==============================================================================

# ========================TASK MAKE INCREMENT OSOBY=============================

    task_make_increment_tcitation_affiliations = MakeIncrementOperator(
        task_id="task_make_increment_tcitation_affiliations",
        source_name=EZOP_SRC_NAME,
        table_name=EZOP_STG_TABLE_TCITATION_AFFILIATIONS,
        conn_id=EZOP_STG_CONN_ID,
        db=EZOP_STG_DB,
        is_psc=True
    )

# ========================TASK MAKE INCREMENT OSOBY=============================

    task_make_increment_tcitation_authors = MakeIncrementOperator(
        task_id="task_make_increment_tcitation_authors",
        source_name=EZOP_SRC_NAME,
        table_name=EZOP_STG_TABLE_TCITATION_AUTHORS,
        conn_id=EZOP_STG_CONN_ID,
        db=EZOP_STG_DB,
        is_psc=True
    )

# ========================TASK MAKE INCREMENT OSOBY=============================

    task_make_increment_torganizations = MakeIncrementOperator(
        task_id="task_make_increment_torganizations",
        source_name=EZOP_SRC_NAME,
        table_name=EZOP_STG_TABLE_TORGANIZATIONS,
        conn_id=EZOP_STG_CONN_ID,
        db=EZOP_STG_DB,
        is_psc=True
    )

# ==============================================================================
# ================================DEPENDENCIES==================================
# ==============================================================================

    # task_is_tekns_done = BranchPythonOperator(
    #     task_id='task_is_tekns_done',
    #     python_callable=is_tekns_done,
    #     provide_context=True,
    #     dag=dag
    # )
    #
    # task_get_tekns_done = PythonOperator(
    #     task_id='task_get_tekns_done',
    #     python_callable=wait_tekns_done,
    #     provide_context=True,
    #     dag=dag
    # )
    #
    # task_trigger_kos = TriggerDagRunOperator(
    #     task_id='task_trigger_kos',
    #     trigger_dag_id='kos_dag',
    #     conf={'dag_trigger_name': 'ezop_dag'},
    #     dag=dag
    # )


# ==============================================================================
# =================================TARGET=======================================
# ==============================================================================

# =======TASK LOAD T_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR_REL==========

    task_load_t_externiorganizacnijednotka_externicitaceautor_rel = LoadToTargetOperator(
        task_id="task_load_t_externiorganizacnijednotka_externicitaceautor_rel",
        #===conections===
        stg_conn_id=EZOP_STG_CONN_ID,
        stg_db=EZOP_STG_DB,
        tg_conn_id=EZOP_TG_CONN_ID,
        tg_db=EZOP_TG_DB,
        #===table info===
        si_schema=EZOP_SI_SCHEMA,
        si_table=EZOP_SI_TABLE_TCITATION_AFFILIATIONS,
        si_columns=EZOP_SI_COLUMNS_TCITATION_AFFILIATIONS,
        tg_schema=EZOP_TG_SCHEMA,
        tg_table=EZOP_TG_TABLE_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        tg_columns=EZOP_TG_COLUMNS_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        #===info for transforamtions===
        columns_to_concat=EZOP_TG_COLUMNS_TO_CONCAT_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        tg_dtypes=EZOP_TG_COLUMNS_DTYPES_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        str_columns=EZOP_TG_COLUMNS_STRING_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        timestamp_columns=EZOP_TG_COLUMNS_TIMESTAMP_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        init_load=False,
        bk_en_cz=EZOP_TG_BK_EN_CZ_EXTERNIORGANIZACNIJEDNOTKA_EXTERNICITACEAUTOR,
        process_data=None
    )

# ====================TASK LOAD T_VVVS_EXTERNI_CITACE_AUTOR=====================

    task_load_t_vvvs_externi_citace_autor = LoadToTargetOperator(
        task_id="task_load_t_vvvs_externi_citace_autor",
        #===conections===
        stg_conn_id=EZOP_STG_CONN_ID,
        stg_db=EZOP_STG_DB,
        tg_conn_id=EZOP_TG_CONN_ID,
        tg_db=EZOP_TG_DB,
        #===table info===
        si_schema=EZOP_SI_SCHEMA,
        si_table=EZOP_SI_TABLE_TCITATION_AUTHORS,
        si_columns=EZOP_SI_COLUMNS_TCITATION_AUTHORS,
        tg_schema=EZOP_TG_SCHEMA,
        tg_table=EZOP_TG_TABLE_EXTERNI_CITACE_AUTOR,
        tg_columns=EZOP_TG_COLUMNS_EXTERNI_CITACE_AUTOR,
        #===info for transforamtions===
        columns_to_concat=EZOP_TG_COLUMNS_TO_CONCAT_EXTERNI_CITACE_AUTOR,
        tg_dtypes=EZOP_TG_COLUMNS_DTYPES_EXTERNI_CITACE_AUTOR,
        str_columns=EZOP_TG_COLUMNS_STRING_EXTERNI_CITACE_AUTOR,
        timestamp_columns=EZOP_TG_COLUMNS_TIMESTAMP_EXTERNI_CITACE_AUTOR,
        init_load=False,
        bk_en_cz=EZOP_TG_BK_EN_CZ_EXTERNI_CITACE_AUTOR,
        process_data=None
    )

# =================TASK LOAD T_ORGJ_ORGANIZACNI_JEDNOTKA_EXTERNI================

    task_load_t_orgj_organizacni_jednotka_externi = LoadToTargetOperator(
        task_id="task_load_t_orgj_organizacni_jednotka_externi",
        #===conections===
        stg_conn_id=EZOP_STG_CONN_ID,
        stg_db=EZOP_STG_DB,
        tg_conn_id=EZOP_TG_CONN_ID,
        tg_db=EZOP_TG_DB,
        #===table info===
        si_schema=EZOP_SI_SCHEMA,
        si_table=EZOP_SI_TABLE_TORGANIZATIONS,
        si_columns=EZOP_SI_COLUMNS_TORGANIZATIONS,
        tg_schema=EZOP_TG_SCHEMA,
        tg_table=EZOP_TG_TABLE_ORGANIZACNI_JEDNOTKA,
        tg_columns=EZOP_TG_COLUMNS_ORGANIZACNI_JEDNOTKA,
        #===info for transforamtions===
        columns_to_concat=EZOP_TG_COLUMNS_TO_CONCAT_ORGANIZACNI_JEDNOTKA,
        tg_dtypes=EZOP_TG_COLUMNS_DTYPES_ORGANIZACNI_JEDNOTKA,
        str_columns=EZOP_TG_COLUMNS_STRING_ORGANIZACNI_JEDNOTKA,
        timestamp_columns=EZOP_TG_COLUMNS_TIMESTAMP_ORGANIZACNI_JEDNOTKA,
        init_load=False,
        bk_en_cz=EZOP_TG_BK_EN_CZ_ORGANIZACNI_JEDNOTKA,
        process_data=process_data_orgj_externi.process_data
    )

#===============================================================================

    task_start_ezop >> [task_load_tcitation_affiliations,
                        task_load_tcitation_authors,
                        task_load_torganizations]

    task_load_tcitation_affiliations >> task_make_increment_tcitation_affiliations
    task_load_tcitation_authors >> task_make_increment_tcitation_authors
    task_load_torganizations >> task_make_increment_torganizations

    task_make_increment_tcitation_affiliations >> task_load_t_externiorganizacnijednotka_externicitaceautor_rel
    task_make_increment_tcitation_authors >> task_load_t_vvvs_externi_citace_autor
    task_make_increment_torganizations >>  task_load_t_orgj_organizacni_jednotka_externi

    # task_is_tekns_done >> [task_load_t_orgj_organizacni_jednotka_externi, task_trigger_kos]
    # task_trigger_kos >> task_get_tekns_done >> task_load_t_orgj_organizacni_jednotka_externi

    # task_start_ezop >> task_load_tcitation_affiliations
