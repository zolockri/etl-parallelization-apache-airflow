import smtplib
from email.message import EmailMessage
import os
from dotenv import load_dotenv

_ = load_dotenv()

def send_email(subject: str, message: str):
    try:
        email_address = os.environ.get("DWH_EMAIL_ADDRESS")
        email_password = os.environ.get("DWH_EMAIL_PASSWORD")

        if email_address is None or email_password is None:
            print("Wrong or missing email address/password")
            return False

        # create email
        msg = EmailMessage()
        msg['Subject'] = subject
        msg['From'] = os.environ.get("DWH_EMAIL_SENDERS_NAME")
        msg['To'] = os.environ.get("DWH_EMAIL_RECIPIENT_MAIL_ADDR")
        msg.set_content(message)

        # send email
        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
            smtp.login(email_address, email_password)
            smtp.send_message(msg)
        return True
    except Exception as e:
        print("Problem sending email")
        print(str(e))
    return False
