from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.oracle.hooks.oracle import OracleHook

def create_connector(conn_id, db):
    print(conn_id)
    print(db)
    if db is None:
        return create_connector_oracle(oracle_conn_id=conn_id)
    else:
        return create_connector_pg(pg_conn_id=conn_id, db=db)


def create_connection(conn_id, db):
    if db is None:
        return create_connection_oracle(oracle_conn_id=conn_id)
    else:
        return create_connection_pg(pg_conn_id=conn_id, db=db)


#===============================================================================

def create_connection_pg(pg_conn_id: str, db: str):
    if not pg_conn_id or not db:
        raise ValueError('Connection id or database name cannot be empty or None')
    try:
        pg_hook = PostgresHook(
            postgres_conn_id=pg_conn_id,
            database=db
        )
        pg_engine = pg_hook.get_sqlalchemy_engine()
        pg_engine = pg_engine.execution_options(fast_executemany=True)
        return pg_engine.connect()
    except Exception as e:
        print(f"Failed to connect to database {db} with connection id {pg_conn_id}")
        raise Exception(str(e))

def create_connector_pg(pg_conn_id: str, db: str):
    if not pg_conn_id or not db:
        raise ValueError('Connection id or database name cannot be empty or None')

    try:
        pg_hook = PostgresHook(
            postgres_conn_id=pg_conn_id,
            database=db
        )
        pg_conn = pg_hook.get_conn()
        return pg_conn
    except Exception as e:
        print(f"Failed to connect to database {db} with connection id {pg_conn_id}")
        raise Exception(str(e))

#===============================================================================

def create_connection_oracle(oracle_conn_id: str):
    if not oracle_conn_id:
        raise ValueError('Connection id cannot be empty or None')
    try:
        oracle_hook = OracleHook(
            oracle_conn_id=oracle_conn_id
        )
        oracle_engine = oracle_hook.get_sqlalchemy_engine()

        return oracle_engine.connect()
    except Exception as e:
        raise Exception(f"Failed to connect to database with connection id {oracle_conn_id}")

def create_connector_oracle(oracle_conn_id: str):
    if not oracle_conn_id:
        raise ValueError('Connection id cannot be empty or None')
    try:
        oracle_hook = OracleHook(
            oracle_conn_id=oracle_conn_id
        )
        oracle_conn = oracle_hook.get_conn()
        return oracle_conn
    except Exception as e:
        print(f"Failed to connect to database with connection id {oracle_conn_id}")
        raise Exception(str(e))
