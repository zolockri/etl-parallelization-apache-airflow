from typing import Dict, Any, Iterator, Optional
import io
class StringIteratorIO(io.TextIOBase):
    def __init__(self, iter: Iterator[str]):
        self._iter = iter
        self._buff = ''

    def readable(self) -> bool:
        return True

    def _read1(self, n: Optional[int] = None) -> str:
        while not self._buff:
            try:
                self._buff = next(self._iter)
            except StopIteration:
                break
        ret = self._buff[:n]
        self._buff = self._buff[len(ret):]
        return ret

    def read(self, n: Optional[int] = None) -> str:
        line = []
        if n is None or n < 0:
            while True:
                m = self._read1()
                if not m:
                    break
                line.append(m)
        else:
            while n > 0:
                m = self._read1(n)
                if not m:
                    break
                n -= len(m)
                line.append(m)
        return ''.join(line).replace('"', '""""').replace('"', '\"')
        # return ''.join(line).replace('"', '""""').replace('"', '\"') # Toto je fix od makara.adamko, ktory konecne po 10h prace FUGNUJE a makara.adamko si moze ist kupit ifon.
        # Rucne strucne: Vyuzil som znalosti z KOP (kombinatoricky som to optimalizoval)
        # Funguje to nasledovne: Z navodu na intenete som vycital, ze ked chceme mat 1 " tak je ich potreba vyescapeovat """" (az 4 uvozovkami) no a had takto 4 dvojite uvodzovky zdrcne na 1, preto je potreba ich este vyescapeovat spatnym lomitkom.
        # Lenze len 1 spatne lomitko nestaci (python to somehow intrepretuje stle zle, preto som skusil dat dve \\ a to uz islo!)


    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._iter = None
