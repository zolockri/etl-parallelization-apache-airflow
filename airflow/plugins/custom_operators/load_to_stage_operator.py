from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from datetime import datetime

from custom_operators.helpers.connections import *
from custom_operators.helpers.dwh_email import *

import pandas as pd
import numpy as np
import tempfile

class LoadToStageOperator(BaseOperator):
    @apply_defaults
    def __init__(
            self,
            src_conn_id,
            src_db,
            src_schema,
            src_table,
            src_columns,
            src_array_size,
            stg_conn_id,
            stg_db,
            stg_schema,
            stg_table,
            stg_columns,
            stg_columns_dtype,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.src_conn_id = src_conn_id
        self.src_db = src_db
        self.src_schema = src_schema
        self.src_table = src_table
        self.src_columns = src_columns
        self.src_array_size = src_array_size
        self.stg_conn_id = stg_conn_id
        self.stg_db = stg_db
        self.stg_schema = stg_schema
        self.stg_table = stg_table
        self.stg_columns = stg_columns
        self.stg_columns_dtype = stg_columns_dtype

    def execute(self, context):
        self.load_table()


#===============================================================================
    def extract(self, src_cursor, src_stmt, stg_cursor, copy_to_stage_stmt):
        from psycopg2 import sql
        import re

        # to detect that first chunk is loaded into truncated table
        is_loading = False

        str_cols = [i for i, (k, v) in enumerate(self.stg_columns_dtype.items()) if v == 'object']

        # function to replace newlines and carriage returns with a space // hot fix to deal with new lines
        regex_replace = np.vectorize(lambda x: re.sub(r"[\n\r]", " ", x))


        # set array size for source cursor
        src_cursor.arraysize = self.src_array_size # adjust this value based on needs and resources
        if self.src_db is None: # oracle DBMS doesn't have DB name, but prefetch is for oracle only
            src_cursor.prefetchrows = self.src_array_size + 1


        src_cursor.execute(src_stmt)

        while True:
            src_chunk = src_cursor.fetchmany(src_cursor.arraysize)
            if not src_chunk:
                break

            src_chunk_np = np.array(src_chunk, dtype=object)

            # clean strings in fetch data
            for col in str_cols:
                src_chunk_np[:, col] = src_chunk_np[:, col].astype(str)
                src_chunk_np[:, col] = regex_replace(src_chunk_np[:, col])


            print(f"Inserting data to {self.stg_schema}.{self.stg_table}")
            if is_loading:
                data_iterator = self.get_data_iterator(self.chunk_with_md5(src_chunk_np))
                stg_cursor.copy_expert(copy_to_stage_stmt, data_iterator)
            else:
                trunc_stmt = sql.SQL("TRUNCATE TABLE {stg_schema}.{stg_table};").format(
                    stg_schema=sql.Identifier(self.stg_schema),
                    stg_table=sql.Identifier(self.stg_table)
                )
                stg_cursor.execute(trunc_stmt)
                data_iterator = self.get_data_iterator(self.chunk_with_md5(src_chunk_np))
                stg_cursor.copy_expert(copy_to_stage_stmt, data_iterator)
                is_loading=True


#===============================================================================

    def load_table_in_chunks(self, **kwargs):
        import numpy as np
        from psycopg2 import sql

        is_loading = False
        chunk_size = 10**6

        src_stmt = sql.SQL("""SELECT {select_column} FROM\
        {src_schema}.{src_table}""").format(
            select_column=sql.SQL(',').join(sql.Identifier(n) for n in self.src_columns),
            src_schema=sql.Identifier(self.src_schema),
            src_table=sql.Identifier(self.src_table))

        copy_to_stage_stmt = sql.SQL("""COPY {stg_schema}.{stg_table} \
        ({columns}) FROM STDIN WITH (FORMAT csv, DELIMITER E'\x1f', NULL 'None')""").format(
            stg_schema=sql.Identifier(self.stg_schema),
            stg_table=sql.Identifier(self.stg_table),
            columns=sql.SQL(',').join(sql.Identifier(n) for n in self.stg_columns))

        self.extract(kwargs['src_cursor'], src_stmt, kwargs['stg_cursor'], copy_to_stage_stmt)


#===============================================================================

    def load_table(self, **kwargs): # source_table_name, source_schema_name, source_columns, stage_table_name, stage_schema_name
        from datetime import datetime

        with create_connector_pg(self.stg_conn_id, self.stg_db) as stg_conn:
            try:
                src_conn = create_connector(self.src_conn_id, self.src_db)
                src_cursor = src_conn.cursor()
                stg_cursor = stg_conn.cursor()
                print(f"Loading to table {self.stg_schema}.{self.stg_table} STARTED")
                self.load_table_in_chunks(
                    src_cursor=src_cursor,
                    stg_cursor=stg_cursor)

                self.alter_owner_to_big(stg_cursor)
                stg_cursor.close()
                stg_conn.commit()
                print(f"Table {self.stg_schema}.{self.stg_table} SUCCESSFULLY loaded")
            except Exception as error:
                stg_conn.rollback()
                print("Error: ", error)
                send_email(subject=f"{self.src_conn_id}: loading to stage FAILED",
                    message=f"{self.src_conn_id}: loading to stage for table {self.stg_schema}.{self.stg_table} FAILED with error: {error} at {datetime.now()}")
                raise
            finally:
                stg_cursor.close() # with closure closes connection
                src_cursor.close()
                src_conn.close()

#===============================================================================
#===============================================================================

    def md5_string(self, string):
        import hashlib
        return hashlib.md5(string.encode('utf-8')).hexdigest()

#===============================================================================

    def chunk_with_md5(self, chunk):
        # calculate MD5 hashes for each row of the NumPy array
        md5_vectorized = np.vectorize(self.md5_string)

        md5_hashes = md5_vectorized(np.apply_along_axis(lambda x: ''.join(x.astype(str)), 1, chunk))

        # append MD5 hashes as a new column to the NumPy array
        chunk_array_with_md5 = np.column_stack((chunk, md5_hashes))

        return chunk_array_with_md5

#===============================================================================

    def get_data_iterator(self, data):
        from custom_operators.helpers.stringiteratorio import StringIteratorIO
        # data = [tuple(row) for row in data.itertuples(index=False)]
        return StringIteratorIO((
            "\x1f".join(map(str, row)) + '\n')
            for row in data
        )

#===============================================================================

    def alter_owner_to_big(self, cursor):
        from psycopg2 import sql
        alter_stmt = sql.SQL("ALTER TABLE {schema_name}.{table_name} \
        OWNER TO big;").format(
            schema_name=sql.Identifier(self.stg_schema),
            table_name=sql.Identifier(self.stg_table)
        )

        cursor.execute(alter_stmt)
