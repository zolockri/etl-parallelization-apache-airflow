from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults

from custom_operators.helpers.connections import *
from custom_operators.helpers.dwh_email import *

import psycopg2
from datetime import datetime

class MakeIncrementOperator(BaseOperator):
    @apply_defaults
    def __init__(
            self,
            source_name,
            table_name,
            conn_id,
            db,
            is_psc,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.source_name = source_name
        self.table_name = table_name
        self.conn_id = conn_id
        self.db = db
        self.is_psc = is_psc

    def execute(self, context):
        self.make_increment()

#===============================================================================

    def make_increment(self, **kwargs):
        if self.is_psc:
            self.create_clean_pre_stage()
        self.inc_clear_state_flag(schema_name='si_' + self.source_name)
        self.get_M_N_D_flags(schema_name='ps_' + self.source_name)

#===============================================================================

    def create_clean_pre_stage(self, **kwargs):
        with create_connector_pg(self.conn_id, self.db) as conn:
            try:
                cursor = conn.cursor()
                proc = 'psc_' + self.source_name + '.create_' + self.table_name
                # cursor.execute("SELECT public.inc_clear_state_flag_schema_table_args")
                cursor.callproc(proc)
                print(f"Pre-stage for psc_{self.source_name}.{self.table_name} created")
                # cursor.close()
                conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                print("Error: ", error)
                conn.rollback()
                send_email(subject= proc + 'FAILED',
                    message=f"Pre-stage for psc_{self.source_name}.{self.table_name} FAILED with error: {error} at {datetime.now()}")
                raise


#===============================================================================

    def inc_clear_state_flag(self, **kwargs):
        from datetime import datetime
        with create_connector_pg(self.conn_id, self.db) as conn:
            try:
                cursor = conn.cursor()

                # cursor.execute("SELECT public.inc_clear_state_flag_schema_table_args")
                cursor.callproc("public.inc_clear_state_flag_schema_table_args",
                                                (kwargs['schema_name'], self.table_name))
                print(f"State flags for table {kwargs['schema_name']}.{self.table_name} cleared")
                # cursor.close()
                conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                print("Error: ", error)
                conn.rollback()
                send_email(subject='inc_clear_state_flag FAILED',
                    message=f"inc_clear_state_flag for si_{self.source_name}.{self.table_name} FAILED with error: {error} at {datetime.now()}")
                raise

#===============================================================================

    def get_M_N_D_flags(self, **kwargs):
        with create_connector_pg(self.conn_id, self.db) as conn:
            try:
                cursor = conn.cursor()

                print(f"Calling inc_find_modified_in_pre_stage_schema_table_args with arguments: {kwargs['schema_name']}, {self.table_name}")
                # raise
                cursor.callproc("public.inc_find_modified_in_pre_stage_schema_table_args",
                                                (kwargs['schema_name'], self.table_name))
                print(f"State flags M for table {kwargs['schema_name']}.{self.table_name} got")

                cursor.callproc("public.inc_find_new_in_pre_stage_schema_table_args",
                                                (kwargs['schema_name'], self.table_name))
                print(f"State flags N for table {kwargs['schema_name']}.{self.table_name} got")

                cursor.callproc("public.inc_find_deleted_in_pre_stage_schema_table_args",
                                                (kwargs['schema_name'], self.table_name))

                print(f"State flags D for table {kwargs['schema_name']}.{self.table_name} got")
                conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                print("Error: ", error)
                conn.rollback()
                send_email(subject='get_M_N_D_flags FAILED',
                    message=f"get_M_N_D_flags for si_{self.source_name}.{self.table_name} FAILED with error: {error}")
                raise
