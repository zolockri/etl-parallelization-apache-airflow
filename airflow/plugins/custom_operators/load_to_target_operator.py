from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults

from concurrent.futures import ThreadPoolExecutor

from custom_operators.helpers.connections import *
from custom_operators.helpers.dwh_email import *

class LoadToTargetOperator(BaseOperator):

    # initialize the operator with the required parameters
    @apply_defaults
    def __init__(
            self,
            #===conections===
            stg_conn_id,
            stg_db,
            tg_conn_id,
            tg_db,
            #===table info===
            si_schema,
            si_table,
            si_columns,
            tg_schema,
            tg_table,
            tg_columns,
            #===info for transforamtions===
            columns_to_concat,
            tg_dtypes,
            str_columns,
            timestamp_columns,
            init_load,
            bk_en_cz,
            process_data,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.stg_conn_id = stg_conn_id
        self.stg_db = stg_db
        self.tg_conn_id = tg_conn_id
        self.tg_db = tg_db
        self.si_schema = si_schema
        self.si_table = si_table
        self.si_columns = si_columns
        self.tg_schema = tg_schema
        self.tg_table = tg_table
        self.tg_columns = tg_columns
        self.columns_to_concat = columns_to_concat
        self.tg_dtypes = tg_dtypes
        self.str_columns = str_columns
        self.timestamp_columns = timestamp_columns
        self.init_load = init_load
        self.bk_en_cz = bk_en_cz
        self.process_data = process_data

    # execute the operator
    def execute(self, context):
        self.load_table_to_tg()

#===============================================================================

    # load data to the target table
    def load_table_to_tg(self, **kwargs):
        from datetime import datetime
        with create_connector_pg(self.tg_conn_id , self.tg_db) as tg_conn:
            try:
                stg_conn = create_connector_pg(self.stg_conn_id, self.stg_db)
                stg_cursor = stg_conn.cursor()
                tg_cursor = tg_conn.cursor()
                print(f"Loading to table {self.tg_schema}.{self.tg_table} STARTED")
                self.load_to_target(
                    stg_cursor=stg_cursor,
                    tg_cursor=tg_cursor
                    )
                tg_cursor.close()
                tg_conn.commit()
                print(f"Table {self.tg_schema}.{self.tg_table} SUCCESSFULLY loaded")
            except Exception as error:
                tg_conn.rollback()
                print("Error: ", error)
                send_email(subject="Loading to target FAILED",
                    message=f"Loading to target for table {self.tg_schema}.{self.tg_table} FAILED with error: {error} at {datetime.now()}")
                raise
            finally:
                tg_cursor.close()
                stg_cursor.close()
                stg_conn.close()

#===============================================================================

    # load data from the source table to the target table
    def load_to_target(self, **kwargs):
        import pandas as pd

        chunk_size = 10**5
        offset = 0
        si_column_aliases_lst = [f"{key} as {value}" for key, value in self.si_columns.items()]

        # create the SELECT statement for the input table


        # process and load data in chunks
        while True:

            table_input_stmt = sql.SQL("""SELECT {select_column} FROM\
            {si_schema}.{si_table}\
             WHERE state IS NOT NULL LIMIT {chunk_size} OFFSET {offset}""").format(
                select_column=sql.SQL(',').join(sql.Identifier(n) for n in si_column_aliases_lst),
                src_schema=sql.Identifier(self.si_schema),
                src_table=sql.Identifier(self.si_table),
                chunk_size=sql.Identifier(chunk_size),
                offset=sql.Identifier(offset))

            kwargs['stg_cursor'].arraysize = 5 * 10**5
            kwargs['stg_cursor'].execute(table_input_stmt)
            chunk = kwargs['stg_cursor'].fetchmany(kwargs['stg_cursor'].arraysize)
            offset += chunk_size
            if not chunk:
                break

            chunk_df = pd.DataFrame(chunk, columns=[desc[0] for desc in kwargs['stg_cursor'].description])

            if self.process_data is None:
                processed_data = chunk_df
            else:
                processed_data = self.process_data(self, data=chunk_df)

            self.load_data(data=processed_data, tg_cursor=kwargs['tg_cursor'])


#===============================================================================

    def load_data(self, data, tg_cursor):
        # divide data into N, M, D groups by state
        data['state'] = data['state'].astype(str)
        grouped_by_state = data.groupby('state')
        states = set(data['state'].unique())

        # load new records
        if 'N' in states:
            new = grouped_by_state.get_group('N')
            self.load_new( new_data=new, tg_cursor=tg_cursor)
        print("NEW records loaded")

        # load deleted records
        if 'D' in states:
            deleted = grouped_by_state.get_group('D')
            self.load_deleted(deleted, self.bk_en_cz[1], tg_cursor)
        print("DELETED records loaded")

        # load modified records
        if 'M' in states:
            modified = grouped_by_state.get_group('M')
            self.load_modified(modified_data=modified, tg_cursor=tg_cursor)

        print("MODIFIED records loaded")

#===============================================================================

    def load_new(self, new_data, tg_cursor):
        from psycopg2 import sql
        import numpy as np
        new_data['last_update'] = new_data['last_update'].dt.floor('D')
        new_data.loc[:, 'version'] = 1

        if self.init_load:
            new_data['date_from'] = '1900-01-01 00:00:00'
        else:
            new_data.loc[:, 'date_from'] = new_data['last_update'].copy()

        new_data['date_to'] = '2199-12-31 00:00:00'
        last_update_col = new_data.pop('last_update')
        new_data['last_update'] = last_update_col
        new_to_load = new_data[self.tg_columns].copy()

        # process float columns since pandas int can't contain None / possible refactor: .astype('Int64')
        float_cols = new_to_load.select_dtypes(include=['float64']).columns
        for col in float_cols:
            # new_to_load[col] = new_to_load[col].apply(lambda x: '{:.0f}'.format(x) if x.is_integer() else 'None')
            new_to_load[col] = new_to_load[col].astype('Int64')

        # new_to_load = new_to_load.replace(r'\n', ' ', regex=True)

        # replace missing values with None for COPY stmt to load as NULL
        new_to_load = new_to_load.fillna(np.nan).replace([np.nan], [None])

        # process timestamp columns due to this: https://github.com/pandas-dev/pandas/issues/29024
        for ts_col in self.timestamp_columns:
            new_to_load[ts_col] = new_to_load[ts_col].astype(object).where(new_to_load[ts_col].notnull(), None)

        # creste COPY stmt and execute for bulk loading
        copy_to_target_stmt = sql.SQL("""COPY {tg_schema}.{tg_table} \
        ({columns}) FROM STDIN WITH (FORMAT csv, DELIMITER E'\x1f', NULL 'None')""").format(
            tg_schema=sql.Identifier(self.tg_schema),
            tg_table=sql.Identifier(self.tg_table),
            columns=sql.SQL(',').join(sql.Identifier(n) for n in self.tg_columns))


        data_iterator = self.get_data_iterator(new_to_load)
        tg_cursor.copy_expert(copy_to_target_stmt, data_iterator)


#===============================================================================

    def load_modified(self, modified_data, tg_cursor):
        import numpy as np
        def process_row(row_data, conn):
            try:
                with conn.cursor() as cursor:
                    self.call_inc_hist_m(row_data['concated'], row_data['active'], row_data['last_update'], cursor)
                conn.commit()
            except Exception as e:
                conn.rollback()
                print(f"Error processing row {row_data}: {e}")
                raise

        def process_rows(data_split, conn):
            for _, row in data_split.iterrows():
                process_row(row, conn)

        def process_rows_in_parallel(modified_data, num_connections):
            # Divide the DataFrame into smaller DataFrames
            data_splits = np.array_split(modified_data, num_connections)

            # Create connections
            connections = []
            for _ in range(num_connections):
                try:
                    conn = create_connector_pg(self.tg_conn_id, self.tg_db)
                    connections.append(conn)
                except Exception as e:
                    print(f"Error creating connection: {e}")
                    raise

            # Process records in parallel using ThreadPoolExecutor
            with ThreadPoolExecutor(max_workers=num_connections) as executor:
                # Submit tasks to the executor and wait for their completion
                futures = [executor.submit(process_rows, data_split, connections[i]) for i, data_split in enumerate(data_splits)]
                for future in futures:
                    future.result()

            # Close connections after processing all rows
            for conn in connections:
                try:
                    conn.commit()
                    conn.close()
                except Exception as e:
                    print(f"Error closing connection: {e}")
                    raise

        modified_data = self.concat_fields(modified_data)

        # Define the number of connections
        num_connections = 20
        process_rows_in_parallel(modified_data, num_connections)


#===============================================================================

    # call procedure in postgres, callproc is blocking -- checked
    def call_inc_hist_m(self, concated, active, last_update, tg_cursor):
        # print(concated)
        tg_cursor.callproc("public.inc_historize_case_m", (concated, self.tg_table, active, last_update))

#===============================================================================

    def call_delete_update(self, bk_str, bk, tg_cursor):
        from psycopg2 import sql
        delete_update_stmt = sql.SQL("UPDATE dwh.{tg_table} \
                                      SET date_to = CURRENT_TIMESTAMP \
                                      WHERE {bk_str} = {bk} \
                                      AND (date_to = '2199-12-31 00:00:00');").format(
                                      tg_table=sql.Identifier(str(self.tg_table)),
                                      bk_str=sql.Identifier(str(bk_str)),
                                      bk=sql.Literal(bk)
                                      )
        tg_cursor.execute(delete_update_stmt)

#===============================================================================

    def load_deleted(self, deleted_data, bk_str, tg_cursor):
        deleted_data.apply(lambda row: self.call_delete_update(bk_str, row[bk_str], tg_cursor), axis=1)

#===============================================================================
#===============================================================================

    def database_lookup(self, lookup_info, data, in_str, **kwargs):
        import pandas as pd


        with create_connection_pg(self.stg_conn_id, self.stg_db) as stage_conn:
            try:
                # get unique values and create a string for the SQL query
                unique_values = data[lookup_info['bk_en_cz'][1]].unique()
                unique_values_str = ', '.join(f"('{v}')" for v in unique_values)
                columns = [f"{key} as {value}" for key, value in lookup_info['cols_to_select'].items()]

                # create and execute the lookup SQL query

                lookup_query = sql.SQL("""SELECT {select_column} FROM\
                {lookup_schema}.{lookup_table}\
                 WHERE {smth} IS NOT NULL LIMIT {chunk_size} OFFSET {offset}""").format(
                    select_column=sql.SQL(',').join(sql.Identifier(n) for n in columns),
                    lookup_schema=sql.Identifier(lookup_info['lookup_schema']),
                    lookup_table=sql.Identifier(lookup_info['lookup_table']),
                    smth=sql.Identifier(lookup_info['bk_en_cz'][0]),
                    in_str=sql.Identifier(in_str)
                    constraint=sql.Identifier(lookup_info['constraint']))

                lookup_res = pd.read_sql_query(lookup_query, con=stage_conn)

                # set indexes for data and lookup_res DataFrames
                data = data.set_index(lookup_info['bk_en_cz'][1])
                lookup_res = lookup_res.set_index(lookup_info['bk_en_cz'][1])

                # join data and lookup_res DataFrames
                res_chunk = data.join(lookup_res, how='left')

                return res_chunk.reset_index()

            except Exception as e:
                email_sub = f"Lookup in table {lookup_info['lookup_table']} FAILED"
                email_msg = str(e)
                send_email(email_sub, email_msg)
                raise Exception(f"Lookup in table {lookup_info['lookup_table']} FAILED")

#===============================================================================

    def get_data_iterator(self, data):
        from custom_operators.helpers.stringiteratorio import StringIteratorIO

        data = [tuple(row) for row in data.itertuples(index=False)]
        return StringIteratorIO((
                "\x1f".join(map(str, row)) + '\n')
                for row in data
            )


#===============================================================================

    def concat_fields(self, data):
        import pandas as pd
        print(data.info())
        # Set the delimiter for concatenating fields
        delimiter = '\\x1f'

        # Replace float columns with integers if the value is an integer
        float_cols = data.select_dtypes(include=['float64']).columns
        for col in float_cols:
            data[col] = data[col].apply(lambda x: '{:.0f}'.format(x) if x.is_integer() else None)

        # Replace null timestamp columns with None
        for ts_col in self.timestamp_columns:
            data[ts_col] = data[ts_col].astype(object).where(data[ts_col].notnull(), None)

        # Replace null values with empty strings and convert data to string type
        data.fillna('', inplace=True)
        data = data.astype(str)

        # Strip whitespace from string columns
        data[self.str_columns] = data[self.str_columns].apply(lambda x: x.str.strip())

        # Concatenate the specified columns using the delimiter and replace problematic characters
        data['concated'] = data[self.columns_to_concat].apply(
            lambda x: (delimiter.join(x).replace("'", "''")).replace("%", "%%"),
            axis=1)

        # Return the modified DataFrame with the specified columns
        return pd.DataFrame(data, columns=['concated', 'active', 'last_update'])
