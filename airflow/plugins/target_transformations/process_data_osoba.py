
def process_data(obj, data, **kwargs):
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print(data)
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

    data['username'] = data['uzivatelske_jmeno'].str.upper()
    lst = list(set(data['username'].tolist()))
    in_str = ', '.join(f"'{username}'" for username in lst)

    lookup_info = {
            'cols_to_select' : {
                'username': 'username',
                'osoba_id': 'kos_osoba_peridno'
            },
            'bk_en_cz' : ('username', 'username'), #('username', )
            'lookup_schema' : 'ps_kos',
            'lookup_table' : 'tusers_airflow',
            'constraint' : '1=1',
            'col_to_add' : 'kos_osoba_peridno'
        }
    processed_data = obj.database_lookup(lookup_info=lookup_info, data=data, in_str=in_str)
    #
    # # processed_data['uzivatelske_jmeno'] = processed_data['uzivatelske_jmeno'].str.lower()
    # # import modin.pandas as pd
    # processed_data.to_csv('/home/zolockri/Documents/FIT/BP/lookup_osoba.csv')
    # # raise
    return processed_data
