
def process_data(obj, data, **kwargs):
    lookup_info = {
            'cols_to_select' : {
                'classification_id': 'klasifikace_bk',
                'name': 'nazev_cs'
            },
            'bk_en_cz' : obj.bk_en_cz,
            'lookup_schema' : 'ps_grades',
            'lookup_table' : 'classification_text_airflow',
            'constraint' : 'language_tag = \'cs\'',
            'col_to_add' : 'nazev_cs'
        }
    lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
    in_str = ', '.join(str(x) for x in lst)
    processed_data = obj.database_lookup(lookup_info=lookup_info, data=data, in_str=in_str)
    lookup_info = {
            'cols_to_select' : {
                'classification_id': 'klasifikace_bk',
                'name': 'nazev_en'
            },
            'bk_en_cz' : obj.bk_en_cz,
            'lookup_schema' : 'ps_grades',
            'lookup_table' : 'classification_text_airflow',
            'constraint' : 'language_tag = \'en\'',
            'col_to_add' : 'nazev_en'
        }
    lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
    in_str = ', '.join(str(x) for x in lst)
    processed_data = obj.database_lookup(lookup_info=lookup_info,
                                     data=processed_data, in_str=in_str)

    # other transformations; in this case replace boolean value to varchar Y/N
    # boolean_to_YN = lambda x: 'Y' if x else 'N'
    boolean_to_YN = lambda x: 'Y' if x is True else ('N' if x is False else None)

    boolean_columns = processed_data.select_dtypes(include='bool').columns
    processed_data[boolean_columns] = processed_data[boolean_columns].apply(
                                            lambda x: x.apply(boolean_to_YN))
    return processed_data
