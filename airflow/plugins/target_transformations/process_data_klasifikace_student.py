def process_data(obj, data, **kwargs): # should be manually adjust for every table
    import pandas as pd
    lookup_info = {
            'cols_to_select' : {
                'classification_user_id': 'classification_user_id',
                'personal_number': 'fk_student_umap_peridno'
            },
            'bk_en_cz' : ('classification_user_id', 'classification_user_id'),
            'lookup_schema' : 'ps_grades',
            'lookup_table' : 'classification_user_airflow',
            'constraint' : '1 = 1',
            'col_to_add' : 'fk_student_umap_peridno'
        }

    lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
    in_str = ', '.join(str(x) for x in lst)
    processed_data = obj.database_lookup(lookup_info=lookup_info, data=data, in_str = in_str)
    # processed_data.to_csv('/home/zolockri/Documents/userlookup.csv')
    # raise

    lookup_info = {
            'cols_to_select' : {
                'classification_id': 'fk_klasifikace',
                'value_type': 'type'
            },
            'bk_en_cz' : ('classification_id', 'fk_klasifikace'),
            'lookup_schema' : 'ps_grades',
            'lookup_table' : 'classification_airflow',
            'constraint' : '1 = 1',
            'col_to_add' : 'type'
        }
    lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
    in_str = ', '.join(str(x) for x in lst)
    processed_data = obj.database_lookup(lookup_info=lookup_info, data=processed_data, in_str = in_str)

    # processed_data.to_csv('/home/zolockri/Documents/userlookup.csv')
    # raise
    # other transformations; in this case replace boolean value to varchar Y/N
    # boolean_to_YN = lambda x: 'Y' if x else 'N'
    boolean_to_YN = lambda x: 'Y' if x is True else ('N' if x is False else None)

    boolean_columns = processed_data.select_dtypes(include='bool').columns
    processed_data[boolean_columns] = processed_data[boolean_columns].apply(
                                            lambda x: x.apply(boolean_to_YN))

    are_loaded = [False, False]

    processed_data['type'] = processed_data['type'].astype(str)
    grouped_by_type = processed_data.groupby('type')
    types = set(processed_data['type'].unique())

    if 'BOOLEAN' in types:
        are_loaded[0] = True
        boolean_type = grouped_by_type.get_group('BOOLEAN')

        lookup_info = {
                'cols_to_select' : {
                    'student_classification_id': 'klasifikace_student_bk',
                    'value': 'hodnota'
                },
                'bk_en_cz' : ('student_classification_id', 'klasifikace_student_bk'),
                'lookup_schema' : 'ps_grades',
                'lookup_table' : 'boolean_student_classification_airflow',
                'constraint' : '1 = 1',
                'col_to_add' : 'hodnota'
            }
        lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
        in_str = ', '.join(str(x) for x in lst)
        boolean_type = obj.database_lookup(lookup_info=lookup_info, data=boolean_type, in_str = in_str)
        boolean_type['hodnota'] = boolean_type['hodnota'].replace({True: 'Y', False: 'N'})
        processed_data = boolean_type
    # boolean_type.to_csv('/home/zolockri/Documents/bool.csv')

    if 'NUMBER' in types:
        are_loaded[1] = True
        number_type = grouped_by_type.get_group('NUMBER')

        lookup_info = {
                'cols_to_select' : {
                    'student_classification_id': 'klasifikace_student_bk',
                    'value': 'hodnota'
                },
                'bk_en_cz' : ('student_classification_id', 'klasifikace_student_bk'),
                'lookup_schema' : 'ps_grades',
                'lookup_table' : 'number_student_classification_airflow',
                'constraint' : '1 = 1',
                'col_to_add' : 'hodnota'
            }
        lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
        in_str = ', '.join(str(x) for x in lst)
        number_type = obj.database_lookup(lookup_info=lookup_info, data=number_type, in_str = in_str)
        number_type['hodnota'] = number_type['hodnota'].astype(str)
        if are_loaded[0]:
            processed_data = pd.concat([processed_data, number_type], ignore_index=True, axis=0)
        else:
            processed_data = number_type


    if 'STRING' in types:
        string_type = grouped_by_type.get_group('STRING')

        lookup_info = {
                'cols_to_select' : {
                    'student_classification_id': 'klasifikace_student_bk',
                    'value': 'hodnota'
                },
                'bk_en_cz' : ('student_classification_id', 'klasifikace_student_bk'),
                'lookup_schema' : 'ps_grades',
                'lookup_table' : 'string_student_classification_airflow',
                'constraint' : '1 = 1',
                'col_to_add' : 'hodnota'
            }
        lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
        in_str = ', '.join(str(x) for x in lst)
        string_type = obj.database_lookup(lookup_info=lookup_info, data=string_type, in_str = in_str)

        if are_loaded[0] or are_loaded[1]:
            processed_data = pd.concat([processed_data, string_type], ignore_index=True, axis=0)
        else:
            processed_data = string_type



    # processed_data = pd.concat([boolean_type, number_type, string_type], ignore_index=True, axis=0)
    return processed_data
