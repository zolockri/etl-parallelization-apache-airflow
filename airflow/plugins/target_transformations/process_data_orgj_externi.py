
def process_data(obj, data, **kwargs):
    lookup_info = {
            'cols_to_select' : {
                'ns': 'externi_organizacni_jednotka_bk',
                'nsidno': 'fk_organizacni_jednotka'
            },
            'bk_en_cz' : ('ns', 'externi_organizacni_jednotka_bk'),
            'lookup_schema' : 'ps_kos',
            'lookup_table' : 'tekns_orig',
            'constraint' : '1=1',
            'col_to_add' : 'fk_organizacni_jednotka'
        }

    lst = list(set(data[lookup_info['bk_en_cz'][1]].tolist()))
    in_str = ', '.join(str(x) for x in lst)
    processed_data = obj.database_lookup(lookup_info=lookup_info, data=data, in_str=in_str)
    # import modin.pandas as pd
    return processed_data
